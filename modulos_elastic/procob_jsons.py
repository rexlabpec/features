import csv
# import requests
import json
from elasticsearch import Elasticsearch, helpers
import time
from datetime import datetime

rows = []
with open('procob.csv', newline='') as f:
    reader = csv.reader(f, delimiter=';', quotechar='"')
    i = 0
    d_max = datetime.strptime('2000-01-01 00:00:00.0', '%Y-%m-%d %H:%M:%S.%f')
    for row in reader:
        rows.append(row)
        if i != 0:
            # print(i)
            try:
                d = datetime.strptime(row[2], '%Y-%m-%dT%H:%M:%S')
            except:
                d = datetime.strptime(row[2], '%Y-%m-%dT%H:%M:%S.%f')
            if d > d_max:
                d_max = d
        i += 1
f.close

total = i
date = d_max.isoformat()

host = '34.204.134.254'
es = Elasticsearch([{'host': host, 'port': 9200}], http_auth="elastic:xupaclearsale")

response = helpers.scan(client=es,
                        scroll='2m',
                        index='bureau_v5',
                        query={"query": {"bool": {"must": [{"range": {"date": {"gte": date}}}]}}})

j = total
for i in response:
    print(j + 1)
    # print(i)
    try:
        x = int(i['_id'])
        x = True
    except:
        x = False
    if x:
        rows.append([])
        rows[j].append(str(i))
        rows[j].append(i['_id'])
        rows[j].append(i['_source']['date'])
    j += 1

with open('procob.csv', 'w', newline='') as csvfile:
    writer = csv.writer(csvfile, delimiter=';')
    # writer.writerow(['json','document_number', 'name', 'date_of_birth', 'cellphone1', 'cellphone2', 'cellphone3', 'zip_code1', 'zip_code2', 'zip_code3', 'email1', 'email2', 'email3', 'insertion_date'])

    for k in range(len(rows)):
        try:
            writer.writerow(rows[k])
        except UnicodeEncodeError:
            print(rows[k])






