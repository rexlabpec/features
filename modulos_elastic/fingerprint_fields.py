import csv
import json
from flatten_json import flatten
from elasticsearch import Elasticsearch, helpers
import time
from datetime import datetime
import pandas as pd
from pandas.io.json import json_normalize

host = '34.204.134.254'
# host = '54.236.54.105'
es = Elasticsearch([{'host': host, 'port': 9200}], http_auth="elastic:xupaclearsale")

responses = helpers.scan(client=es,
                        scroll='10m',
                        index='device',
                        query={"query": {"bool": {"must": []}}})
head = []
head_real = []
i = 0
table = []

output_file = 'fingerprint_data_normalized.csv'

data = []
for r in responses:
    i += 1
    print(i)
    data.append(r.get('_source'))
dic_flattened = json_normalize(data)
pd.DataFrame(dic_flattened).to_csv(output_file, index=False, encoding='utf-8', sep=';')
