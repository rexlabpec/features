import csv
#import requests
import json
from elasticsearch import Elasticsearch, helpers
import time
from datetime import datetime

rows = []
with open('elastic-teste.csv', newline='') as f:
    reader = csv.reader(f, delimiter=';', quotechar='"')
    i = 0
    d_max = datetime.strptime('2000-01-01 00:00:00.0', '%Y-%m-%d %H:%M:%S.%f')
    for row in reader:
        rows.append(row)
        if i != 0:
            #print(i)
            try:
                d = datetime.strptime(row[7], '%Y-%m-%dT%H:%M:%S')
            except:
                d = datetime.strptime(row[7], '%Y-%m-%dT%H:%M:%S.%f')
            if d > d_max:
                d_max = d
        i += 1
f.close

total = i
date = d_max.isoformat()


host = '34.204.134.254'
es = Elasticsearch([{'host': host, 'port': 9200}], http_auth="elastic:xupaclearsale")

responses = helpers.scan(client = es,
                        scroll = '2m',
                        index = 'bureau',
                        query = {"query":{"bool":{"must": [{"range":{"date":{"gte": date}}}]}}})

j = total
for i in responses:
    print(j+1)
    #print(i)
    try:
        x = int(i['_id'])
        x = True
    except:
        x = False
    if x:
        rows.append([])
        rows[j].append(str(i))
        rows[j].append(i['_id'])
        response = i['_source']
        try:
            nome = response['personal_data'][0]['name']
            date_birth = response['personal_data'][0]['date_of_birth']
        except IndexError:
            nome = ''
            date_birth = ''
        insertion_date = response['date']
        ceps_responses = response['addresses']
        try:
            cels_responses = response['phones']
            cels_exist = True
        except KeyError:
            cels_exist = False
        emails_responses = response['emails']
        rows[j].append(nome)
        rows[j].append(date_birth)
        # Celulares
        aux = ''
        if cels_exist:
            cellphones_responses = cels_responses['cellphone']
            for k in range(len(cellphones_responses)):
                try:
                    aux = aux + ',' + cellphones_responses[k]['full_number']
                except IndexError:
                    aux.append('')
            residentials_responses = cels_responses['residential']
            for k in range(len(residentials_responses)):
                try:
                    aux = aux + ',' + residentials_responses[k]['full_number']
                except IndexError:
                    aux = aux + ''
            others_responses = cels_responses['others']
            for k in range(len(others_responses)):
                try:
                    aux = aux + ',' + others_responses[k]['full_number']
                except IndexError:
                    aux = aux + ''
        rows[j].append(aux)
        # CEPs
        aux = ''
        for k in range(len(ceps_responses)):
            try:
                aux = aux + ',' + ceps_responses[k]['zip_code']
            except IndexError:
                aux = aux + ''
        rows[j].append(aux)
        # Endereço
        # aux = []
        # for k in range(len(ceps_responses)):
        #     try:
        #         aux.append(ceps_responses[k]['city'])
        #     except IndexError:
        #         aux.append('')
        # aux = set(aux)
        # aux = ','.join(aux)
        # rows[j].append(aux)
        # Emails
        aux = ''
        for k in range(len(emails_responses)):
            try:
                aux = aux + ',' + emails_responses[k]
            except IndexError:
                aux = aux + ''
        rows[j].append(aux)
        rows[j].append(insertion_date)
        #print(type(insertion_date))
        j += 1

with open('elastic-teste.csv', 'w', newline='') as csvfile:
    
    writer = csv.writer(csvfile, delimiter = ';')
    #writer.writerow(['json','document_number', 'name', 'date_of_birth', 'cellphone1', 'cellphone2', 'cellphone3', 'zip_code1', 'zip_code2', 'zip_code3', 'email1', 'email2', 'email3', 'insertion_date'])
    
    for k in range(len(rows)):
        try:
            writer.writerow(rows[k])
        except UnicodeEncodeError:
            print(rows[k])





            
