WITH last_event AS (
SELECT  "Order".id AS order_id,
        max("OrderEvent".id)  AS last_order_event_id,
        max("PaymentEvent".id)  AS last_payment_event_id

FROM "Order"  LEFT JOIN "OrderEvent"  ON "Order".id = "OrderEvent".order_id
              LEFT JOIN "OrderEventType" ON "OrderEvent".order_event_type_id = "OrderEventType".id
              LEFT JOIN "Payment" ON "Order".id = "Payment".order_id
              LEFT JOIN "PaymentEvent" ON "Payment".id = "PaymentEvent".payment_id

WHERE order_event_type_id != 1
      AND payment_event_type_id != 1

GROUP BY "Order".id)

SELECT
  "Order".id AS "Order_ID",
  "Client".client_key AS "Client_Key",
  "Client".name AS "Client_Name",
  "Order".order_date AS "transaction_date",
  cast("Payment".amount as DECIMAL)/100 AS "total_amount",
  "OrderEventType".name AS "Antifraud_Decision" ,
  "PaymentEventType".name AS "Issuer_Decision",
  "PaymentEvent".event_date as "updated_date",
  order_json->'source'->'antifraud_status'->'reasons' AS "reasons",

  order_json->'source'->'sentinela_id' AS "index",
  order_json->'source'->'order_id' AS "order_key",

  order_json->'source'->'seller'->'name' AS "seller_name",
  order_json->'source'->'seller'->'seller_id' AS "seller_id",


  order_json->'source'->'shipping'->'address'->'city' AS "city",
  order_json->'source'->'shipping'->'address'->'state' AS "state",
  order_json->'source'->'shipping'->'address'->'street' AS "street",
  order_json->'source'->'shipping'->'address'->'number' AS "street_number",
  order_json->'source'->'shipping'->'address'->'neighborhood' AS "neighborhood",
  replace(cast(order_json -> 'source' -> 'shipping' -> 'address' -> 'zip_code' as TEXT),'-', '') AS "cep",

  order_json->'source'->'customer'->'documents'->0->'document_type' AS "document_type",
  order_json->'source'->'customer'->'documents'->0->'number' AS "cpf",
  order_json->'source'->'customer'->'email' AS "email",
  order_json->'source'->'customer'->'name' AS "buyer_name",
  order_json->'source'->'customer'->'phones'->0->'country_code' AS "ddi",
  order_json->'source'->'customer'->'phones'->0->'area_code' AS "ddd",
  order_json->'source'->'customer'->'phones'->0->'number' AS "cell_number",


  order_json->'source'->'device'->'ip' AS "ip",
  order_json->'source'->'device'->'platform' AS "platform",
  order_json->'source'->'device'->'session_id' AS "session_id",


  order_json->'source'->'payment'->'transactions'->0->'card_name' AS "cardname",
  order_json->'source'->'payment'->'transactions'->0->'bin' AS "bin",
  order_json->'source'->'payment'->'transactions'->0->'last_4' AS "last_4",
  order_json->'source'->'payment'->'transactions'->0->'expiration_date' AS "exp_date",


  order_json->'source'->'products' as "products",
  "OrderEventType".id AS "AF Decision"

FROM  last_event
LEFT JOIN "Order" ON last_event.order_id = "Order".id
LEFT JOIN "OrderEvent" ON last_order_event_id = "OrderEvent".id
LEFT JOIN "PaymentEvent" ON last_payment_event_id = "PaymentEvent".id
LEFT JOIN "OrderEventType" ON "OrderEvent".order_event_type_id = "OrderEventType".id
LEFT JOIN "PaymentEventType" ON "PaymentEvent".payment_event_type_id = "PaymentEventType".id
LEFT JOIN "Payment" ON last_event.order_id = "Payment".order_id
LEFT JOIN "Client" ON "Order".client_id = "Client".id

WHERE 1=1
--AND "Client".name LIKE '%Pedidos%'
--ORDER BY "Order".order_date DESC
