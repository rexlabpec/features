import numpy as np
import pandas as pd
from dateutil.relativedelta import relativedelta
import csv
import time
import json


def levenshtein(s1, s2):
    if len(s1) < len(s2):
        return levenshtein(s2, s1)

    # len(s1) >= len(s2)
    if len(s2) == 0:
        return len(s1)

    previous_row = range(len(s2) + 1)
    for i, c1 in enumerate(s1):
        current_row = [i + 1]
        for j, c2 in enumerate(s2):
            insertions = previous_row[
                             j + 1] + 1  # j+1 instead of j since previous_row and current_row are one character longer
            deletions = current_row[j] + 1  # than s2
            substitutions = previous_row[j] + (c1 != c2)
            current_row.append(min(insertions, deletions, substitutions))
        previous_row = current_row

    return [previous_row[-1], max(len(s1),len(s2))]

def retirar_acentos(text):
    text = text.replace('á', 'a')
    text = text.replace('é', 'e')
    text = text.replace('í', 'i')
    text = text.replace('ó', 'o')
    text = text.replace('ú', 'u')

    text = text.replace('à', 'a')
    text = text.replace('è', 'e')
    text = text.replace('ì', 'i')
    text = text.replace('ò', 'o')
    text = text.replace('ù', 'u')

    text = text.replace('ã', 'a')
    text = text.replace('ẽ', 'e')
    text = text.replace('ĩ', 'i')
    text = text.replace('õ', 'o')
    text = text.replace('ũ', 'u')

    text = text.replace('â', 'a')
    text = text.replace('ê', 'e')
    text = text.replace('î', 'i')
    text = text.replace('ô', 'o')
    text = text.replace('û', 'u')

    text = text.replace('ä', 'a')
    text = text.replace('ë', 'e')
    text = text.replace('ï', 'i')
    text = text.replace('ö', 'o')
    text = text.replace('ü', 'u')

    text = text.replace('ç', 'c')
    text = text.replace('ñ', 'n')

    text = text.replace('Á', 'A')
    text = text.replace('É', 'E')
    text = text.replace('Í', 'I')
    text = text.replace('Ó', 'O')
    text = text.replace('Ú', 'U')

    text = text.replace('À', 'A')
    text = text.replace('È', 'E')
    text = text.replace('Ì', 'I')
    text = text.replace('Ò', 'O')
    text = text.replace('Ù', 'U')

    text = text.replace('Ã', 'A')
    text = text.replace('Ẽ', 'E')
    text = text.replace('Ĩ', 'I')
    text = text.replace('Õ', 'O')
    text = text.replace('Ũ', 'U')

    text = text.replace('Â', 'A')
    text = text.replace('Ê', 'E')
    text = text.replace('Î', 'I')
    text = text.replace('Ô', 'O')
    text = text.replace('Û', 'U')

    text = text.replace('Ä', 'A')
    text = text.replace('Ë', 'E')
    text = text.replace('Ï', 'I')
    text = text.replace('Ö', 'O')
    text = text.replace('Ü', 'U')

    text = text.replace('Ç', 'C')
    text = text.replace('Ñ', 'N')

    return text

def vinculo_procob_telefone(dado, dado_procob):
    for request_phones in dado_procob:
        for request in request_phones:
            if dado == request['full_number']:
                return True
    return False

def vinculo_procob_cep(dado, dado_procob):
    for request in dado_procob:
        if dado == request['zip_code']:
            return True
    return False

def vinculo_procob_nome1(name, nome_procob):
    quantidade = 0
    nomes = name.split()
    for nome in nomes:
        try:
            procob = nome_procob.lower()
        except AttributeError:
            procob = ''
        if retirar_acentos(nome.lower()) in procob:
            quantidade += 1
    return quantidade/len(nomes)

def vinculo_procob_nome2(name, nome_procob):
    quantidade = 0
    nomes = retirar_acentos(name.strip().lower()).split()
    nomes_procob = retirar_acentos(nome_procob.strip().lower()).split()
    j = 0
    for nome in nomes:
        dmin = [100, 0]
        for i in range(j,len(nomes_procob)):
            d = levenshtein(nome, nomes_procob[i])
            if d[0] < dmin[0]:
                dmin = d
                j = i + 1
        if dmin[1] != 0:
            quantidade += 1 - (dmin[0]/dmin[1])
    return quantidade/len(nomes_procob)

def vinculos(dados, client_name, validar_telefone, validar_email, validar_cep):
    procob = pd.read_csv('modulos/procob.csv', encoding='latin1', dtype={'document_number': 'str', 'name': 'str'}, sep=';')
    vinculos_nome1 = [0 for i in range(len(dados.index))]
    vinculos_nome2 = [0 for i in range(len(dados.index))]
    vinculos_rip = [0 for i in range(len(dados.index))]
    vinculos_telefone = [0 for i in range(len(dados.index))]
    vinculos_email = [0 for i in range(len(dados.index))]
    vinculos_end_email = [0 for i in range(len(dados.index))]
    vinculos_cep = [0 for i in range(len(dados.index))]
    vinculos_age = [0 for i in range(len(dados.index))]
    return_procob = [0 for i in range(len(dados.index))]
    head = ['index', 'vinculo_nome', 'vinculo_nome_levenshtein', 'rip', 'vinculo_telefone', 'vinculo_email', 'vinculo_endereco_email', 'vinculo_cep', 'age', 'return_procob']
    index = dados.ix[:, 'index']
    for i in range(len(dados.index)):
        start = time.time()
        print('vinculos', i)
        cpf = str(dados['cpf'][i])
        name = dados['buyer_name'][i]
        transaction_date = dados['transaction_date'][i]
        if validar_telefone:
            telefone = dados['cellphone'][i]
        else:
            telefone = np.nan
        if validar_email:
            email = dados['email'][i]
            end_email = dados['email'][i].split('@')[0]
        else:
            email = np.nan
        if validar_cep:
            cep = dados['cep'][i]
        else:
            cep = np.nan

        try:
            # aux = procob[procob.document_number == cpf]['json'].iloc[0].replace('\\x', '')
            aux = procob[procob.document_number == cpf]['json'].iloc[0]
            # print(aux)
            procob_response = eval(aux)
            nome_procob = procob_response['_source']['personal_data'][0]['name']
            try:
                telefone_procob = [procob_response['_source']['phones']['residential'], procob_response['_source']['phones']['others'], procob_response['_source']['phones']['cellphone']]
            except KeyError:
                telefone_procob = [[{"full_number": "0"}]]
            email_procob = [x.lower() for x in procob_response['_source']['emails']]
            cep_procob = procob_response['_source']['addresses']
            vinculos_nome1[i] = vinculo_procob_nome1(name, nome_procob)
            vinculos_nome2[i] = vinculo_procob_nome2(name, nome_procob)
            vinculos_rip[i] = procob_response['_source']['personal_data'][0]['rip']
            vinculos_telefone[i] = vinculo_procob_telefone(telefone, telefone_procob)
            try:
                vinculos_email[i] = email.lower() in email_procob
            except AttributeError:
                vinculos_email[i] = email in email_procob
            end_email_procob = [x.split('@')[0].lower() for x in email_procob]
            try:
                vinculos_end_email[i] = end_email.lower() in end_email_procob
            except AttributeError:
                vinculos_end_email[i] = end_email in end_email_procob
            vinculos_cep[i] = vinculo_procob_cep(cep, cep_procob)
            vinculos_age[i] = relativedelta(transaction_date, procob_response['_source']['personal_data'][0]['date_of_birth']).years
            return_procob[i] = True
        except IndexError:
            vinculos_nome1[i] = 0
            vinculos_nome2[i] = 0
            vinculos_rip[i] = False
            vinculos_telefone[i] = False
            vinculos_email[i] = False
            vinculos_end_email[i] = False
            vinculos_cep[i] = False
            vinculos_age[i] = 0
            return_procob[i] = False
        end = time.time()
        print(end - start)
    table = zip(index, vinculos_nome1, vinculos_nome2, vinculos_rip, vinculos_telefone, vinculos_email, vinculos_end_email, vinculos_cep, vinculos_age, return_procob)
    with open('dataset/'+client_name+'/features/vinculos.csv', 'w', newline='') as f:
        writer = csv.writer(f, delimiter = ';')
        writer.writerow(head)
        for row in table:
            writer.writerow(row)