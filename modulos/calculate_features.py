import csv
import time
import pandas as pd
import psycopg2
from dateutil.relativedelta import relativedelta
from date_operations import parse_date, compare_dates, subtract_dates, get_recurrency, get_recurrency_per_client
from transactions import TransactionCsv, ChargebackGerencial, TransactionGerencial


def save_history(trx):
    global history

    trx_info = {'id': trx.trx_id,
                'client': trx.client,
                'email': trx.email,
                'card': trx.card_id,
                'amount': trx.amount,
                'status': trx.status,
                'date': trx.created_at,
                'document': trx.document_number,
                'ip': trx.ip,
                'card_name': trx.card_name,
                'customer_name': trx.customer_name,
                'zip_code': trx.zip_code,
                'neighborhood': trx.zip_code,
                'cellphone': trx.cellphone,
                'session_id': trx.session_id}

    if trx.email:
        if trx.email in history['emails']:
            history['emails'][trx.email].append(trx_info)
        else:
            history['emails'][trx.email] = [trx_info]

    if trx.card_id:
        if trx.card_id in history['cards']:
            history['cards'][trx.card_id].append(trx_info)
        else:
            history['cards'][trx.card_id] = [trx_info]

    if trx.document_type and trx.document_number:
        if trx.document_number in history['documents'][trx.document_type]:
            history['documents'][trx.document_type][trx.document_number].append(trx_info)
        else:
            history['documents'][trx.document_type][trx.document_number] = [trx_info]

    if trx.ip:
        if trx.ip in history['ips']:
            history['ips'][trx.ip].append(trx_info)
        else:
            history['ips'][trx.ip] = [trx_info]

    if trx.card_name:
        if trx.card_name in history['card_names']:
            history['card_names'][trx.card_name].append(trx_info)
        else:
            history['card_names'][trx.card_name] = [trx_info]

    if trx.zip_code:
        if trx.zip_code in history['zip_codes']:
            history['zip_codes'][trx.zip_code].append(trx_info)
        else:
            history['zip_codes'][trx.zip_code] = [trx_info]

    if trx.neighborhood:
        if trx.neighborhood in history['neighborhoods']:
            history['neighborhoods'][trx.neighborhood].append(trx_info)
        else:
            history['neighborhoods'][trx.neighborhood] = [trx_info]

    if trx.cellphone:
        if trx.cellphone in history['cellphones']:
            history['cellphones'][trx.cellphone].append(trx_info)
        else:
            history['cellphones'][trx.cellphone] = [trx_info]

    if trx.session_id:
        if trx.session_id in history['session_ids']:
            history['session_ids'][trx.session_id].append(trx_info)
        else:
            history['session_ids'][trx.session_id] = [trx_info]

    return history


def save_chargeback(cbk):
    global chargeback

    incoming_date = parse_date(cbk.incoming_date)


    if cbk.email:
        if cbk.email in chargeback['emails']:
            chargeback['emails'][cbk.email].append(incoming_date)
        else:
            chargeback['emails'][cbk.email] = [incoming_date]

    if cbk.expiration_date:
        if cbk.get_cardhash_bin_last4() in chargeback['cards']:
            chargeback['cards'][cbk.get_cardhash_complete()].append(incoming_date)
        else:
            chargeback['cards'][cbk.get_cardhash_complete()] = [incoming_date]
    elif cbk.bin and cbk.last_4:
        if cbk.get_cardhash_bin_last4() in chargeback['cards']:
            chargeback['cards'][cbk.get_cardhash_bin_last4()].append(incoming_date)
        else:
            chargeback['cards'][cbk.get_cardhash_bin_last4()] = [incoming_date]

    if cbk.document_type and cbk.document_number:
        if cbk.document_number in chargeback['documents'][cbk.document_type]:
            chargeback['documents'][cbk.document_type][cbk.document_number].append(incoming_date)
        else:
            chargeback['documents'][cbk.document_type][cbk.document_number] = [incoming_date]

    if cbk.ip:
        if cbk.ip in chargeback['ips']:
            chargeback['ips'][cbk.ip].append(incoming_date)
        else:
            chargeback['ips'][cbk.ip] = [incoming_date]

    if cbk.card_name:
        if cbk.card_name in chargeback['card_names']:
            chargeback['card_names'][cbk.card_name].append(incoming_date)
        else:
            chargeback['card_names'][cbk.card_name] = [incoming_date]

    if cbk.zip_code:
        if cbk.zip_code in chargeback['zip_codes']:
            chargeback['zip_codes'][cbk.zip_code].append(incoming_date)
        else:
            chargeback['zip_codes'][cbk.zip_code] = [incoming_date]

    if cbk.get_fullnumber():
        if cbk.get_fullnumber() in chargeback['cellphones']:
            chargeback['cellphones'][cbk.get_fullnumber].append(incoming_date)
        else:
            chargeback['cellphones'][cbk.get_fullnumber] = [incoming_date]

    if cbk.neighborhood:
        if cbk.neighborhood in chargeback['neighborhoods']:
            chargeback['neighborhoods'][cbk.neighborhood].append(incoming_date)
        else:
            chargeback['neighborhoods'][cbk.neighborhood] = [incoming_date]

    return chargeback


def update_historical_features(trx, feature):
    global history, chargeback

    update = [{'history': history['emails'].get(trx.email),
               'chargeback': chargeback['emails'].get(trx.email),
               'name': 'Email'},
              {'history': history['documents'][trx.document_type].get(trx.document_number),
               'chargeback': chargeback['documents'][trx.document_type].get(trx.document_number),
               'name': 'Document'},
              {'history': history['cards'].get(trx.card_id),
               'chargeback': chargeback['cards'].get(trx.card_id),
               'name': 'Card'},
              {'history': history['ips'].get(trx.ip),
               'chargeback': chargeback['ips'].get(trx.ip),
               'name': 'Ip'},
              {'history': history['zip_codes'].get(trx.zip_code),
               'chargeback': chargeback['zip_codes'].get(trx.zip_code),
               'name': 'ZipCode'},
              {'history': history['neighborhoods'].get(trx.neighborhood),
               'chargeback': chargeback['neighborhoods'].get(trx.neighborhood),
               'name': 'Neighborhood'},
              {'history': history['cellphones'].get(trx.cellphone),
               'chargeback': chargeback['cellphones'].get(trx.cellphone),
               'name': 'Cellphone'},
              {'history': history['customer_names'].get(trx.customer_name),
               'chargeback': chargeback['customer_names'].get(trx.customer_name),
               'name': 'CustomerName'},
              {'history': history['card_names'].get(trx.card_name),
               'chargeback': chargeback['card_names'].get(trx.card_name),
               'name': 'CardName'},
              {'history': history['session_ids'].get(trx.session_id),
               'chargeback': None,
               'name': 'SessionId'}]

    # Compute recurrencies
    created_at = parse_date(trx.created_at)
    client = trx.client
    # Iterate throught email, card & docs history
    for u in update:
        if u['history']:

            diff_emails = {trx.email}
            diff_cards = {trx.card_id}
            diff_docs = {trx.document_number}
            diff_ips = {trx.ip}
            diff_zip_codes = {trx.zip_code}
            diff_neighborhoods = {trx.neighborhood}
            diff_cellphones = {trx.cellphone}
            diff_customer_names = {trx.customer_name}
            diff_card_names = {trx.card_name}

            min_date = created_at
            max_date = None

            # Check each transaction individually
            for history_trx in u['history']:

                history_doc = history_trx['document']
                if history_doc in diff_docs:
                    new_doc = 0
                else:
                    diff_docs.add(history_doc)
                    new_doc = 1

                history_card = history_trx['card']
                if history_card in diff_cards:
                    new_card = 0
                else:
                    diff_cards.add(history_card)
                    new_card = 1

                history_email = history_trx['email']
                if history_email in diff_emails:
                    new_email = 0
                else:
                    diff_emails.add(history_email)
                    new_email = 1

                history_ip = history_trx['ip']
                if history_ip in diff_ips:
                    new_ip = 0
                else:
                    diff_ips.add(history_ip)
                    new_ip = 1

                history_zip_code = history_trx['zip_code']
                if history_zip_code in diff_zip_codes:
                    new_zip_code = 0
                else:
                    diff_zip_codes.add(history_zip_code)
                    new_zip_code = 1

                history_neighborhood = history_trx['neighborhood']
                if history_neighborhood in diff_neighborhoods:
                    new_neighborhood = 0
                else:
                    diff_neighborhoods.add(history_neighborhood)
                    new_neighborhood = 1

                history_cellphone = history_trx['cellphone']
                if history_cellphone in diff_cellphones:
                    new_cellphone = 0
                else:
                    diff_cellphones.add(history_cellphone)
                    new_cellphone = 1

                history_customer_name= history_trx['customer_name']
                if history_customer_name in diff_customer_names:
                    new_customer_name = 0
                else:
                    diff_customer_names.add(history_customer_name)
                    new_customer_name = 1

                history_card_name = history_trx['card_name']
                if history_card_name in diff_card_names:
                    new_card_name = 0
                else:
                    diff_card_names.add(history_card_name)
                    new_card_name = 1

                history_date = parse_date(history_trx['date'])
                history_client = history_trx['client']

                # find min & max date
                if history_date < min_date:
                    min_date = history_date
                if max_date is None:
                    max_date = history_date
                if history_date > max_date:
                    max_date = history_date

                if u['name'] != 'SessionId':
                    # Updated fields inside periods: hour, day, week, month
                    recurrency = get_recurrency(created_at, history_date)
                    # for r in recurrency:
                    #     # count number of transactions
                    #     num_field = 'trx_per_' + u['name'] + '_' + r
                    #     feature[num_field] += 1
                    #
                    #     # total transaction amount during period
                    #     amount_field = 'amount_per_' + u['name'] + '_' + r
                    #     feature[amount_field] += history_trx['amount']
                    #
                    #     # how many documents regarding a parameter
                    #     if u['name'] != 'document':
                    #         doc_field = 'documents_per_' + u['name'] + '_' + r
                    #         feature[doc_field] += new_doc
                    #
                    #     # how many emails regarding a parameter
                    #     if u['name'] != 'email':
                    #         email_field = 'emails_per_' + u['name'] + '_' + r
                    #         feature[email_field] += new_email
                    #
                    #     # how many cards regarding a parameter
                    #     if u['name'] != 'card':
                    #         card_field = 'cards_per_' + u['name'] + '_' + r
                    #         feature[card_field] += new_card
                    #
                    #     # how many cards regarding a parameter
                    #     if u['name'] != 'ip':
                    #         ip_field = 'ips_per_' + u['name'] + '_' + r
                    #         feature[ip_field] += new_ip
                    #
                    #     # how many cards regarding a parameter
                    #     if u['name'] != 'zip_code':
                    #         zip_code_field = 'zip_codes_per_' + u['name'] + '_' + r
                    #         feature[zip_code_field] += new_zip_code
                    #
                    #     # how many cards regarding a parameter
                    #     if u['name'] != 'neighborhood':
                    #         neighborhood_field = 'neighborhoods_per_' + u['name'] + '_' + r
                    #         feature[neighborhood_field] += new_neighborhood
                    #
                    #     # how many cards regarding a parameter
                    #     if u['name'] != 'cellphone':
                    #         cellphone_field = 'cellphones_per_' + u['name'] + '_' + r
                    #         feature[cellphone_field] += new_cellphone
                    #
                    #     # how many cards regarding a parameter
                    #     if u['name'] != 'customer_name':
                    #         customer_name_field = 'customer_names_per_' + u['name'] + '_' + r
                    #         feature[customer_name_field] += new_customer_name
                    #
                    #     # how many cards regarding a parameter
                    #     if u['name'] != 'card_name':
                    #         card_name_field = 'card_names_per_' + u['name'] + '_' + r
                    #         feature[card_name_field] += new_card_name
                    #
                    #     if history_trx['status'] != 'refused':
                    #         status_field = 'num_' + history_trx['status'] + '_per_' + u['name'] + '_' + r
                    #         feature[status_field] += 1

                    recurrency_per_client = get_recurrency_per_client(created_at, history_date, client, history_client)
                    for r in recurrency_per_client:
                        # count number of transactions
                        num_field = 'NumTrxPerClientPer' + u['name'] + r
                        feature[num_field] += 1

                        # total transaction amount during period
                        amount_field = 'AmountTrxPerClientPer' + u['name'] + r
                        feature[amount_field] += history_trx['amount']

                        # how many documents regarding a parameter
                        if u['name'] != 'Document':
                            doc_field = 'NumDocumentsPerClientPer' + u['name'] + r 
                            feature[doc_field] += new_doc

                        # how many emails regarding a parameter
                        if u['name'] != 'Email':
                            email_field = 'NumEmailsPerClientPer' + u['name'] + r
                            feature[email_field] += new_email

                        # how many cards regarding a parameter
                        if u['name'] != 'Card':
                            card_field = 'NumCardsPerClientPer' + u['name'] + r
                            feature[card_field] += new_card

                        # how many cards regarding a parameter
                        if u['name'] != 'Ip':
                            ip_field = 'NumIpsPerClientPer' + u['name'] + r
                            feature[ip_field] += new_ip

                        # how many cards regarding a parameter
                        if u['name'] != 'ZipCode':
                            zip_code_field = 'NumZipCodesPerClientPer' + u['name'] + r
                            feature[zip_code_field] += new_zip_code

                        # how many cards regarding a parameter
                        if u['name'] != 'Neighborhood':
                            neighborhood_field = 'NumNeighborhoodsPerClientPer' + u['name'] + r
                            feature[neighborhood_field] += new_neighborhood

                        # how many cards regarding a parameter
                        if u['name'] != 'Cellphone':
                            cellphone_field = 'NumCellphonesPerClientPer' + u['name'] + r
                            feature[cellphone_field] += new_cellphone

                        # how many cards regarding a parameter
                        if u['name'] != 'CustomerName':
                            customer_name_field = 'NumCustomerNamesPerClientPer' + u['name'] + r
                            feature[customer_name_field] += new_customer_name

                        # how many cards regarding a parameter
                        if u['name'] != 'CardName':
                            card_name_field = 'NumCardNamesPerClientPer' + u['name'] + r
                            feature[card_name_field] += new_card_name

                        if history_trx['status'] != 'Refused':
                            status_field = 'Num' + history_trx['status'] + 'PerClientPer' + u['name'] + r
                            feature[status_field] += 1

                    feature['DaysSinceFirstTransaction' + u['name'] + 'PerClient'] = (created_at - min_date).days
                    if max_date:
                        feature['DaysSinceLastTransaction' + u['name'] + 'PerClient'] = (created_at - max_date).days


                else:
                    # count number of transactions
                    num_field = 'NumTrxPer' + u['name']
                    feature[num_field] += 1

                    # total transaction amount during period
                    amount_field = 'AmountTrxPer' + u['name']
                    feature[amount_field] += history_trx['amount']

                    # how many documents regarding a parameter
                    doc_field ='NumDocumentsPer' + u['name']
                    feature[doc_field] += new_doc

                    # how many emails regarding a parameter
                    email_field = 'NumEmailsPer' + u['name']
                    feature[email_field] += new_email

                    # how many cards regarding a parameter
                    card_field = 'NumCardsPer' + u['name']
                    feature[card_field] += new_card

                    # how many cards regarding a parameter
                    ip_field = 'NumIpsPer' + u['name']
                    feature[ip_field] += new_ip

                    # how many cards regarding a parameter
                    zip_code_field = 'NumZipCodesPer' + u['name']
                    feature[zip_code_field] += new_zip_code

                    # how many cards regarding a parameter
                    cellphone_field = 'NumCellphonesPer' + u['name']
                    feature[cellphone_field] += new_cellphone

                    # how many cards regarding a parameter
                    customer_name_field = 'NumCustomerNamesPer' + u['name']
                    feature[customer_name_field] += new_customer_name

                    # how many cards regarding a parameter
                    card_name_field = 'NumCardNamesPer' + u['name']
                    feature[card_name_field] += new_card_name

            if u['name'] != 'session_id':
                feature['DaysSinceFirstTransaction' + u['name']] = 0

        if u['chargeback']:
            for chargeback_trx in u['chargeback']:
                if compare_dates(chargeback_trx, created_at, 'lte'):
                    new_cbk = 1
                else:
                    new_cbk = 0
                cbk_field = 'NumChargebackPer' + u['name']
                feature[cbk_field] += new_cbk

    return feature


def update_products_features(trx, feature):
    bebidas = ['cerveja', 'vodka', 'jurupinga', 'catuaba', 'whisk', 'chopp', 'vinho', 'smirnoff', 'skol', 'itaipava',
               'absolut', 'long neck', 'black label']
    amount = 0
    amount_alcohol = 0
    quantity_alcohol = 0
    quantity = 0
    for product in trx.products:
        already_pass = False
        item = retirar_acentos(product.get('name')).lower()
        quantity += product.get('quantity')
        amount += (product.get('unit_cost')*product.get('quantity'))
        for bebida in bebidas:
            if bebida in item and not already_pass:
                quantity_alcohol += quantity
                amount_alcohol += amount
                feature['ProductsHaveAlcohol'] = 1
                already_pass = True
        if product.get('unit_cost') >= feature['MaxUnitCost']:
            feature['MaxUnitCost'] = product.get('unit_cost')
    if quantity_alcohol == quantity:
        feature['ProductsHaveOnlyAlcohol'] = 1
    feature['PercentQuantityAlcoholInProducts'] = quantity_alcohol/quantity
    feature['PercentAmountAlcoholInProducts'] = amount_alcohol/amount
    feature['TotalQuantity'] = quantity

    return feature


def retirar_acentos(text):
    text = text.replace('á', 'a')
    text = text.replace('é', 'e')
    text = text.replace('í', 'i')
    text = text.replace('ó', 'o')
    text = text.replace('ú', 'u')

    text = text.replace('à', 'a')
    text = text.replace('è', 'e')
    text = text.replace('ì', 'i')
    text = text.replace('ò', 'o')
    text = text.replace('ù', 'u')

    text = text.replace('ã', 'a')
    text = text.replace('ẽ', 'e')
    text = text.replace('ĩ', 'i')
    text = text.replace('õ', 'o')
    text = text.replace('ũ', 'u')

    text = text.replace('â', 'a')
    text = text.replace('ê', 'e')
    text = text.replace('î', 'i')
    text = text.replace('ô', 'o')
    text = text.replace('û', 'u')

    text = text.replace('ä', 'a')
    text = text.replace('ë', 'e')
    text = text.replace('ï', 'i')
    text = text.replace('ö', 'o')
    text = text.replace('ü', 'u')

    text = text.replace('ç', 'c')
    text = text.replace('ñ', 'n')

    text = text.replace('Á', 'A')
    text = text.replace('É', 'E')
    text = text.replace('Í', 'I')
    text = text.replace('Ó', 'O')
    text = text.replace('Ú', 'U')

    text = text.replace('À', 'A')
    text = text.replace('È', 'E')
    text = text.replace('Ì', 'I')
    text = text.replace('Ò', 'O')
    text = text.replace('Ù', 'U')

    text = text.replace('Ã', 'A')
    text = text.replace('Ẽ', 'E')
    text = text.replace('Ĩ', 'I')
    text = text.replace('Õ', 'O')
    text = text.replace('Ũ', 'U')

    text = text.replace('Â', 'A')
    text = text.replace('Ê', 'E')
    text = text.replace('Î', 'I')
    text = text.replace('Ô', 'O')
    text = text.replace('Û', 'U')

    text = text.replace('Ä', 'A')
    text = text.replace('Ë', 'E')
    text = text.replace('Ï', 'I')
    text = text.replace('Ö', 'O')
    text = text.replace('Ü', 'U')

    text = text.replace('Ç', 'C')
    text = text.replace('Ñ', 'N')

    return text


def levenshtein(s1, s2):
    if len(s1) < len(s2):
        return levenshtein(s2, s1)

    # len(s1) >= len(s2)
    if len(s2) == 0:
        return len(s1)

    previous_row = range(len(s2) + 1)
    for i, c1 in enumerate(s1):
        current_row = [i + 1]
        for j, c2 in enumerate(s2):
            insertions = previous_row[
                             j + 1] + 1  # j+1 instead of j since previous_row and current_row are one character longer
            deletions = current_row[j] + 1  # than s2
            substitutions = previous_row[j] + (c1 != c2)
            current_row.append(min(insertions, deletions, substitutions))
        previous_row = current_row

    return [previous_row[-1], max(len(s1),len(s2))]


def procob_distance_name(name, nome_procob):
    quantidade = 0
    nomes = name.split()
    for nome in nomes:
        try:
            procob = nome_procob.lower()
        except AttributeError:
            procob = ''
        if retirar_acentos(nome.lower()) in procob:
            quantidade += 1
    return quantidade/len(nomes)


def procob_levenshtein_distance_name(name, nome_procob):
    quantidade = 0
    nomes = retirar_acentos(name.strip().lower()).split()
    nomes_procob = retirar_acentos(nome_procob.strip().lower()).split()
    j = 0
    for nome in nomes:
        dmin = [100, 0]
        for i in range(j,len(nomes_procob)):
            d = levenshtein(nome, nomes_procob[i])
            if d[0] < dmin[0]:
                dmin = d
                j = i + 1
        if dmin[1] != 0:
            quantidade += 1 - (dmin[0]/dmin[1])
    return quantidade/len(nomes_procob)


def bureau_link(trx, type):
    global bureau

    if bureau.get(trx.document_number):
        bureau_response = bureau.get(trx.document_number)
        if type == 'CustomerPhoneProcobExact':
            if bureau_response.get('phones'):
                bureau_phones = bureau_response.get('phones')
                phone_type_list = bureau_phones.values()
                for phone_type in phone_type_list:
                    for phone in phone_type:
                        if trx.cellphone == phone.get('full_number'):
                            return 1
            return 0
        elif type == 'CustomerEmailProcobExact':
            if bureau_response.get('emails'):
                bureau_emails = bureau_response.get('emails')
                for email in bureau_emails:
                    if trx.email == email:
                        return 1
            return 0
        elif type == 'CustomerZipCodeProcobExact':
            if bureau_response.get('addresses'):
                bureau_addresses = bureau_response.get('addresses')
                for address in bureau_addresses:
                    if trx.zip_code == address.get('zip_code'):
                        return 1
            return 0
        elif type == 'CustomerNameProcobDistance':
            if bureau_response.get('personal_data'):
                try:
                    bureau_personal_data = bureau_response.get('personal_data')[0]
                    return procob_levenshtein_distance_name(trx.customer_name, bureau_personal_data.get('name'))
                except IndexError:
                    return 0
            return 0
        elif type == 'CustomerNameProcobExact':
            if bureau_response.get('personal_data'):
                try:
                    bureau_personal_data = bureau_response.get('personal_data')[0]
                    return procob_distance_name(trx.customer_name, bureau_personal_data.get('name'))
                except IndexError:
                    return 0
            return 0
    if type == 'name' or type == 'name_levenshtein':
        return 0
    return 0


def compute_features(trx):
    global features, history, chargeback, bureau

    vowels = 'aeiou'
    consonants = 'bcdfghjklmnpqrstvwxyz'
    numbers = '0123456789'
    cpf_state = ['rs', ['df', 'go', 'ms', 'to'], ['pa', 'am', 'ac', 'ap', 'ro', 'rr'], ['ce', 'ma', 'pi'],
                   ['pe', 'rn', 'pb', 'al'], ['ba', 'se'], 'mg', ['rj', 'es'], 'sp', ['pr', 'sc']]

    ddd_state = {
        "99": "MA",
        "98": "MA",
        "97": "AM",
        "96": "AP",
        "95": "RR",
        "94": "PA",
        "93": "PA",
        "92": "AM",
        "91": "PA",
        "88": "CE",
        "87": "PE",
        "86": "PI",
        "85": "CE",
        "83": "PB",
        "82": "AL",
        "81": "PE",
        "79": "SE",
        "77": "BA",
        "75": "BA",
        "74": "BA",
        "73": "BA",
        "72": "BA",
        "71": "BA",
        "69": "RO",
        "68": "AC",
        "67": "MS",
        "66": "MT",
        "65": "MT",
        "64": "GO",
        "63": "TO",
        "62": "GO",
        "61": "DF",
        "55": "RS",
        "54": "RS",
        "53": "RS",
        "51": "RS",
        "49": "SC",
        "48": "SC",
        "47": "SC",
        "46": "PR",
        "45": "PR",
        "44": "PR",
        "43": "PR",
        "42": "PR",
        "41": "PR",
        "38": "MG",
        "37": "MG",
        "35": "MG",
        "34": "MG",
        "33": "MG",
        "32": "MG",
        "31": "MG",
        "28": "ES",
        "27": "ES",
        "24": "RJ",
        "22": "RJ",
        "21": "RJ",
        "19": "SP",
        "18": "SP",
        "17": "SP",
        "16": "SP",
        "15": "SP",
        "14": "SP",
        "13": "SP",
        "12": "SP",
        "11": "SP"
    }
    bebidas = ['cerveja', 'vodka', 'jurupinga', 'catuaba', 'whisk', 'chopp', 'vinho', 'smirnoff', 'skol', 'itaipava',
               'absolut', 'long neck', 'black label']

    feature = {}

    feature['TransactionId'] = trx.trx_id

    # Datetime features
    try:
        date = parse_date(trx.created_at)
        feature['TransactionHour'] = date.hour
        feature['TransactionMinute'] = date.minute
        feature['TransactionDay'] = date.day
        feature['TransactionMonth'] = date.month
        feature['TransactionYear'] = date.year
        feature['TransactionWeekday'] = date.weekday()
    except Exception as ex:
        print('EXCEPTION', ex)

    # Transaction features
    feature['PaymentStatus'] = trx.status
    feature['IsChargeback'] = trx.chargeback
    feature['TotalAmount'] = trx.amount

    # Web platform features
    feature['PlatformWeb'] = 1 if trx.platform == 'web' or trx.platform == 'web_mobile' else 0
    feature['PlatformAndroid'] = 1 if trx.platform == 'android' else 0
    feature['PlatformIphone'] = 1 if trx.platform == 'iphone' or trx.platform == 'ipad' else 0

    # Email features
    end_email = trx.email.split('@')[0] if trx.email else None
    feature['EmailPercentVowel'] = sum(end_email.count(i) for i in vowels)/len(end_email) if trx.email else 0
    feature['EmailPercentConsonant'] = sum(end_email.count(i) for i in consonants)/len(end_email) if trx.email else 0
    feature['EmailPercentNumber'] = sum(end_email.count(i) for i in numbers)/len(end_email) if trx.email else 0
    feature['EmailPercentSpecial'] = 1 - feature['EmailPercentNumber'] - feature['EmailPercentConsonant'] - feature['EmailPercentVowel'] if trx.email else 0
    feature['UniqueCharactersInEmail'] = len(list(set(end_email))) if trx.email else 0

    # Location features
    feature['ReceivedZipCode'] = 1 if trx.zip_code else 0

    # Phone features
    feature['ReceivedCellphone'] = 1 if trx.cellphone else 0

    # Compare states features
    try:
        trx_state = trx.state
        feature['SameStateShippingAndDocument'] = 1 if ((trx_state in cpf_state[int(trx.document_number[8])]) and trx.document_type == 'cpf') else 0
        feature['SameStatePhoneDDDAndDocument'] = 1 if (trx_state == ddd_state[trx.phone_ddd].lower()) else 0
    except:
        feature['SameStateShippingAndDocument'] = 0
        feature['SameStatePhoneDDDAndDocument'] = 0

    # Bureau features
    feature['FoundInProcob'] = 1 if trx.document_number in bureau.keys() else 0
    try:
        feature['RipInProcob'] = 1 if bureau[trx.document_number]['personal_data'][0].get('rip') and bureau.get(trx.document_number) else 0
        bureau_birth_date = bureau[trx.document_number]['personal_data'][0].get('date_of_birth') if bureau.get(trx.document_number) else trx.created_at
        bureau_birth_date = parse_date(bureau_birth_date)
        feature['AgeInProcob'] = relativedelta(trx.created_at, bureau_birth_date).years
    except (IndexError, KeyError):
        feature['RipInProcob'] = 0
        feature['AgeInProcob'] = 0

    links = ['CustomerPhoneProcobExact', 'CustomerEmailProcobExact', 'CustomerZipCodeProcobExact', 'CustomerNameProcobExact', 'CustomerNameProcobDistance']
    for link in links:
        feature['Match'+link] = bureau_link(trx, link)

    variables = ['Card', 'Email', 'Document', 'Ip', 'ZipCode', 'Cellphone', 'CustomerName', 'CardName', 'Neighborhood']
    recurrencies = ['Hour', 'Day', 'Week', 'Month']
    # Recurrence features
    for fixed_var in variables:

        for recurrency in recurrencies:
            feature['NumTrxPerClientPer'+fixed_var+recurrency] = 0
            feature['AmountTrxPerClientPer'+fixed_var+recurrency] = 0
            feature['NumCapturedPerClientPer'+fixed_var+recurrency] = 0
            feature['NumRefundedPerClientPer'+fixed_var+recurrency] = 0
            feature['NumNotAuthorizedPerClientPer'+fixed_var+recurrency] = 0
            # feature['NumRefusedPerClientPer'+fixed_var+recurrency] = 0
            # feature['NumTrxPer'+fixed_var+recurrency] = 0
            # feature['AmountTrxPer'+fixed_var+recurrency] = 0
            # feature['NumCapturedPer'+fixed_var+recurrency] = 0
            # feature['NumRefundedPer'+fixed_var+recurrency] = 0
            # feature['NumNotAuthorizedPer'+fixed_var+recurrency] = 0
            # feature['NumRefusedPer'+fixed_var+recurrency] = 0


        feature['NumChargebackPer'+fixed_var] = 0

        feature['DaysSinceFirstTransaction'+fixed_var+'PerClient'] = 0
        feature['DaysSinceLastTransaction'+fixed_var+'PerClient'] = 0
        feature['FirstTransaction'+fixed_var+'PerClient'] = 1

        for variable_var in variables:
            if variable_var != fixed_var:
                for recurrency in recurrencies:
                    feature['Num'+variable_var+'sPerClientPer'+fixed_var+recurrency] = 1
                    # feature['Num'+variable_var+'sPer'+fixed_var+recurrency] = 1

    # Session id features
    for variable_var in variables:
        feature['Num' + variable_var + 'sPerSessionId'] = 1
        feature['NumTrxPerSessionId'] = 0
        feature['AmountTrxPerSessionId'] = 0

    # Products features
    feature['ProductsHaveAlcohol'] = 0
    feature['ProductsHaveOnlyAlcohol'] = 0
    feature['PercentQuantityAlcoholInProducts'] = 0
    feature['PercentAmountAlcoholInProducts'] = 0
    feature['TotalQuantity'] = 0
    feature['MaxUnitCost'] = 0

    if trx.products:
        feature = update_products_features(trx, feature)

    # Update old features that match recurrence
    feature = update_historical_features(trx, feature)

    # Save ids for fast indexing
    save_history(trx)

    return feature


def load_past_data():
    global features, history, chargeback, bureau

    print('Loading history')

    if history is None:
        history = {
            'cards': {},
            'emails': {},
            'documents': {'cpf': {}, 'cnpj': {}},
            'ips': {},
            'zip_codes': {},
            'neighborhoods': {},
            'cellphones': {},
            'customer_names': {},
            'card_names': {},
            'session_ids': {}
        }


def load_chargeback():
    global features, history, chargeback, bureau

    print('Loading chargeback')

    if chargeback is None:
        chargeback = {
            'cards': {},
            'emails': {},
            'companies': {},
            'documents': {'cpf': {}, 'cnpj': {}},
            'ips': {},
            'zip_codes': {},
            'neighborhoods': {},
            'cellphones': {},
            'customer_names': {},
            'card_names': {}
        }

    start = time.time()
    # Connecting to gerencial
    endpoint = 'rexlab-scooby-replica-a.cujsiitttsed.us-east-1.rds.amazonaws.com'
    db = 'ScoobyDB'
    user = 'pchen'
    password = 'vA6KZiRPNCRVbpQP'
    conn = psycopg2.connect(host=endpoint, database=db, user=user, password=password)
    cursor = conn.cursor()
    query = """
WITH last_event AS (
SELECT  "Order".id AS order_id,
        max("OrderEvent".id)  AS last_order_event_id,
        max("PaymentEvent".id)  AS last_payment_event_id

FROM "Order"  LEFT JOIN "OrderEvent"  ON "Order".id = "OrderEvent".order_id
              LEFT JOIN "OrderEventType" ON "OrderEvent".order_event_type_id = "OrderEventType".id
              LEFT JOIN "Payment" ON "Order".id = "Payment".order_id
              LEFT JOIN "PaymentEvent" ON "Payment".id = "PaymentEvent".payment_id

WHERE order_event_type_id != 1
      AND payment_event_type_id = 5

GROUP BY "Order".id)

SELECT
  "Order".id AS "Order_ID",

  order_json->'source'->'customer'->'address'->'city' AS "city",
  order_json->'source'->'customer'->'address'->'state' AS "state",
  order_json->'source'->'customer'->'address'->'street' AS "street",
  order_json->'source'->'customer'->'address'->'number' AS "street_number",
  order_json->'source'->'customer'->'address'->'neighborhood' AS "neighborhood",
  replace(cast(order_json -> 'source' -> 'customer' -> 'address' -> 'zip_code' as TEXT),'-', '') AS "zip_code",


  order_json->'source'->'customer'->'documents'->0->'document_type' AS "document_type",
  order_json->'source'->'customer'->'documents'->0->'number' AS "document_number",
  order_json->'source'->'customer'->'email' AS "email",
  order_json->'source'->'customer'->'name' AS "customer_name",
  order_json->'source'->'customer'->'phones'->0->'country_code' AS "ddi",
  order_json->'source'->'customer'->'phones'->0->'area_code' AS "ddd",
  order_json->'source'->'customer'->'phones'->0->'number' AS "cell_number",


  order_json->'source'->'device'->'ip' AS "ip",


  order_json->'source'->'payment'->'transactions'->0->'card_name' AS "cardname",
  order_json->'source'->'payment'->'transactions'->0->'bin' AS "bin",
  order_json->'source'->'payment'->'transactions'->0->'last_4' AS "last_4",
  order_json->'source'->'payment'->'transactions'->0->'expiration_date' AS "exp_date",


  "PaymentEvent".event_date as "incoming_date"


FROM  last_event
LEFT JOIN "Order" ON last_event.order_id = "Order".id
LEFT JOIN "OrderEvent" ON last_order_event_id = "OrderEvent".id
LEFT JOIN "PaymentEvent" ON last_payment_event_id = "PaymentEvent".id
LEFT JOIN "OrderEventType" ON "OrderEvent".order_event_type_id = "OrderEventType".id
LEFT JOIN "PaymentEventType" ON "PaymentEvent".payment_event_type_id = "PaymentEventType".id
LEFT JOIN "Payment" ON last_event.order_id = "Payment".order_id
LEFT JOIN "Client" ON "Order".client_id = "Client".id

WHERE 1=1

ORDER BY "Order".order_date DESC"""
    cursor.execute(query)

    # Saving chargebacks in memory
    i = 0
    for line in cursor:
        i += 1
        cbk = ChargebackGerencial(line)
        save_chargeback(cbk)

    # Disconnecting
    cursor.close()
    conn.close()
    print('Loaded', i, 'chargebacks in {0:.2}s'.format(time.time() - start))


def load_bureau():
    global features, history, chargeback, bureau

    print('Loading bureau')

    if bureau is None:
        bureau = dict()

    i = 0
    firstline = True
    start = time.time()
    with open('dataset/bureau.csv', newline='', encoding='latin1') as data:
        reader = csv.reader(data, delimiter=';')
        for record in reader:
            if firstline:
                firstline = False
                continue
            i += 1
            bureau[str(record[1])] = eval(record[0]).get('_source')
    data.close()
    print('Bureau loaded', i, 'documents in {0:.2}s'.format(time.time() - start))


def process_data_csv(path):
    global features, history, chargeback, bureau
    start = time.time()
    header = False
    firstline = True
    cnt = 0
    features_csv = open(path+'_features.csv', 'w', newline='', encoding='utf-8')
    load_bureau()
    load_past_data()
    load_chargeback()
    print('Starting computing features')
    with open(path+'.csv', newline='', encoding='utf-8') as data:
        reader = csv.reader(data, delimiter=',', quotechar='"')
        for record in reader:
            if firstline:
                firstline = False
                continue
            trx = TransactionCsv(record)
            f = compute_features(trx)
            w = csv.DictWriter(features_csv,f.keys())
            if not header:
                w.writeheader()
                header = True
            w.writerow(f)
            cnt += 1
    print('Processed', cnt, 'transactions {0:.2}s'.format(time.time() - start))


def connect():
    global data_cursor, pg_conn
    start = time.time()
    endpoint = 'rexlab-scooby-replica-a.cujsiitttsed.us-east-1.rds.amazonaws.com'
    db = 'ScoobyDB'
    user = 'pchen'
    password = 'vA6KZiRPNCRVbpQP'
    pg_conn = psycopg2.connect(host=endpoint, database=db, user=user, password=password)
    data_cursor = pg_conn.cursor()
    print('Connected', time.time() - start)


def fetch_data(client_name, vertical, start, end):
    global data_cursor, pg_conn

    print('Fetching data')
    if not (start and end):
        start_fetch = time.time()
        with open("../query/" + vertical + ".sql", "r") as myfile:
            query = " ".join(line.strip() for line in myfile if not line.startswith("--"))
        myfile.close()
        query = query + " AND " + '"Client".name LIKE ' + "'%" + client_name + "%' ORDER BY " + '"Order".order_date LIMIT 100'
        data_cursor.execute(query)
        print('Data Fetched', '{0:.2}s'.format(time.time() - start_fetch))


def disconnect():
    global pg_conn, data_cursor
    print('Disconnecting...')
    data_cursor.close()
    pg_conn.close()
    print('Disconnected')


def process_data_gerencial(path):
    global features, history, chargeback, bureau, data_cursor
    start = time.time()
    header = False
    cnt = 0
    features_csv = open(path + '_features.csv', 'w', newline='', encoding='utf-8')
    data_csv = open(path + '.csv', 'w', newline='', encoding='utf-8')
    load_bureau()
    load_past_data()
    load_chargeback()

    print('Starting computing features')
    time_1000 = time.time()
    for record in data_cursor:
        trx = TransactionGerencial(record)
        trx.card_id = trx.get_cardhash()
        trx.cellphone = trx.get_fullnumber()
        f = compute_features(trx)
        w = csv.DictWriter(features_csv, f.keys())
        data_writer = csv.DictWriter(data_csv, trx.__dict__.keys())
        if not header:
            w.writeheader()
            data_writer.writeheader()
            header = True
        w.writerow(f)
        data_writer.writerow(trx.__dict__)
        cnt += 1
        if cnt%1000 == 0:
            print('Processed 1000 transactions {0:.2}s'.format(time.time() - time_1000))
            time_1000 = time.time()
    print('Processed', cnt, 'transactions {0:.2}s'.format(time.time() - start))
