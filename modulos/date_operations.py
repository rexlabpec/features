from dateutil import parser
from datetime import datetime, timedelta

def parse_date(date):
    try:
        date = parser.parse(date)
    except TypeError:
        date = date
    return date


def compare_dates(d1, d2, operator):
    if operator == 'gte':
        return d1.replace(tzinfo=None) >= d2.replace(tzinfo=None)
    elif operator == 'lte':
        return d1.replace(tzinfo=None) <= d2.replace(tzinfo=None)
    else:
        raise(Exception('COMPARE_DATES: Missing operator'))


def subtract_dates(d1, d2):
    return d1.replace(tzinfo=None) - d2.replace(tzinfo=None)


def get_recurrency(trx_date, history_date):
    """
    Define recurrency periods of interest
    """
    assert type(trx_date) == datetime
    assert type(history_date) == datetime

    recurrency_range = []

    # return empty if it is not history
    if compare_dates(history_date, trx_date, 'gte'):
        return recurrency_range

    delta = subtract_dates(trx_date, history_date)

    if delta.days < 1 and delta.seconds/3600 < 1:
        recurrency_range.append('hour')
    if delta.days < 1:
        recurrency_range.append('day')
    if delta.days/7 < 1:
        recurrency_range.append('week')
    if delta.days/30 < 1:
        recurrency_range.append('month')

    return recurrency_range


def get_recurrency_per_client(trx_date, history_date, trx_client, history_client):
    """
    Define recurrency periods of interest for the same client
    """
    assert type(trx_date) == datetime
    assert type(history_date) == datetime
    assert type(trx_client) == str
    assert type(history_client) == str

    recurrency_range = []

    if trx_client.lower() == history_client.lower():
        # return empty if it is not history
        if compare_dates(history_date, trx_date, 'gte'):
            return recurrency_range

        delta = subtract_dates(trx_date, history_date)

        if delta.days < 1 and delta.seconds/3600 < 1:
            recurrency_range.append('Hour')
        if delta.days < 1:
            recurrency_range.append('Day')
        if delta.days/7 < 1:
            recurrency_range.append('Week')
        if delta.days/30 < 1:
            recurrency_range.append('Month')

    return recurrency_range