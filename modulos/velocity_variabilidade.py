import numpy as np
import pandas as pd
from datetime import timedelta
import csv
import time


def velocity_variability_card(dados, tempo, unidade, client_name):
    num_trx = [0 for i in range(len(dados.index))]
    amount_trx = [0 for i in range(len(dados.index))]
    cpf = [0 for i in range(len(dados.index))]
    cardname = [0 for i in range(len(dados.index))]
    email = [0 for i in range(len(dados.index))]
    cellphone = [0 for i in range(len(dados.index))]
    cep = [0 for i in range(len(dados.index))]
    ip = [0 for i in range(len(dados.index))]
    buyername = [0 for i in range(len(dados.index))]
    head = ['index', 'num_trx_per_card_'+str(tempo)+unidade, 'amount_trx_per_card_'+str(tempo)+unidade,
            'num_cpfs_per_card_' + str(tempo)+unidade, 'num_cardnames_per_card_'+str(tempo)+unidade,
            'num_emails_per_card_' + str(tempo)+unidade, 'num_cellphones_per_card_'+str(tempo)+unidade,
            'num_ceps_per_card_'+str(tempo)+unidade,'num_ips_per_card_'+str(tempo)+unidade,
            'num_buyernames_per_card_' + str(tempo) + unidade]
    index = dados.ix[:, 'index']
    if unidade == 'day':
        delta = timedelta(days=tempo)
    else:
        delta = timedelta(hours=tempo)
    for i in range(len(dados.index)):
        start = time.time()
        print('bloco card', tempo, unidade, i)
        card = dados['card'][i]
        transaction_date = dados['transaction_date'][i]
        aux = dados[(dados.transaction_date <= transaction_date) & (dados.transaction_date >= (transaction_date - delta))][
                        ['card','cpf','transaction_date','cardname', 'email','cellphone', 'cep', 'total_amount', 'ip', 'buyer_name']]
        num_trx[i] = len(aux[(aux.card == card)].index)
        amount_trx[i] = aux[(aux.card == card)]['total_amount'].sum()
        cpf[i] = aux[(aux.card == card)]['cpf'].nunique()
        cardname[i] = aux[(aux.card == card)]['cardname'].nunique()
        email[i] = aux[(aux.card == card)]['email'].nunique()
        cellphone[i] = aux[(aux.card == card)]['cellphone'].nunique()
        cep[i] = aux[(aux.card == card)]['cep'].nunique()
        ip[i] = aux[(aux.card == card)]['ip'].nunique()
        buyername[i] = aux[(aux.card == card)]['buyer_name'].nunique()
        end = time.time()
        print(end - start)
    table = zip(index, num_trx,amount_trx, cpf, cardname, email, cellphone, cep, ip, buyername)
    with open('dataset/'+client_name+'/features/velocity_variability_card_'+str(tempo)+unidade+'.csv', 'w', newline='') as f:
        writer = csv.writer(f, delimiter=';')
        writer.writerow(head)
        for row in table:
            writer.writerow(row)

def velocity_variability_cpf(dados, tempo, unidade, client_name):
    num_trx = [0 for i in range(len(dados.index))]
    amount_trx = [0 for i in range(len(dados.index))]
    card = [0 for i in range(len(dados.index))]
    cardname = [0 for i in range(len(dados.index))]
    email = [0 for i in range(len(dados.index))]
    cellphone = [0 for i in range(len(dados.index))]
    cep = [0 for i in range(len(dados.index))]
    ip = [0 for i in range(len(dados.index))]
    buyername = [0 for i in range(len(dados.index))]
    head = ['index', 'num_trx_per_cpf_' + str(tempo) + unidade, 'amount_trx_per_cpf_' + str(tempo) + unidade,
            'num_cards_per_cpf_' + str(tempo) + unidade, 'num_cardnames_per_cpf_' + str(tempo) + unidade,
            'num_emails_per_cpf_' + str(tempo) + unidade, 'num_cellphones_per_cpf_' + str(tempo) + unidade,
            'num_ceps_per_cpf_' + str(tempo) + unidade,'num_ips_per_cpf_'+str(tempo)+unidade,'num_buyernames_per_cpf_'+str(tempo)+unidade]
    index = dados.ix[:, 'index']
    if unidade == 'day':
        delta = timedelta(days=tempo)
    else:
        delta = timedelta(hours=tempo)
    for i in range(len(dados.index)):
        start = time.time()
        print('bloco cpf', tempo, unidade, i)
        cpf = dados['cpf'][i]
        transaction_date = dados['transaction_date'][i]
        aux = dados[(dados.transaction_date <= transaction_date) & (dados.transaction_date >= (transaction_date - delta))][
            ['card', 'cpf', 'transaction_date', 'cardname', 'email', 'cellphone', 'total_amount', 'cep', 'ip', 'buyer_name']]
        num_trx[i] = len(aux[(aux.cpf == cpf)].index)
        amount_trx[i] = aux[(aux.cpf == cpf)]['total_amount'].sum()
        card[i] = aux[(aux.cpf == cpf)]['card'].nunique()
        cardname[i] = aux[(aux.cpf == cpf)]['cardname'].nunique()
        email[i] = aux[(aux.cpf == cpf)]['email'].nunique()
        cellphone[i] = aux[(aux.cpf == cpf)]['cellphone'].nunique()
        cep[i] = aux[(aux.cpf == cpf)]['cep'].nunique()
        ip[i] = aux[(aux.cpf == cpf)]['ip'].nunique()
        buyername[i] = aux[(aux.cpf == cpf)]['buyer_name'].nunique()
        end = time.time()
        print(end - start)
    table = zip(index, num_trx,amount_trx, card, cardname, email, cellphone, cep, ip, buyername)
    with open('dataset/'+client_name+'features/velocity_variability_cpf_'+str(tempo)+unidade+'.csv', 'w', newline='') as f:
        writer = csv.writer(f, delimiter = ';')
        writer.writerow(head)
        for row in table:
            writer.writerow(row)

def velocity_variability_email(dados, tempo, unidade, client_name):
    num_trx = [0 for i in range(len(dados.index))]
    amount_trx = [0 for i in range(len(dados.index))]
    card = [0 for i in range(len(dados.index))]
    cardname = [0 for i in range(len(dados.index))]
    cpf = [0 for i in range(len(dados.index))]
    cellphone = [0 for i in range(len(dados.index))]
    cep = [0 for i in range(len(dados.index))]
    ip = [0 for i in range(len(dados.index))]
    buyername = [0 for i in range(len(dados.index))]
    head = ['index', 'num_trx_per_email_' + str(tempo) + unidade, 'amount_trx_per_email_' + str(tempo) + unidade,
            'num_cards_per_email_' + str(tempo) + unidade, 'num_cardnames_per_email_' + str(tempo) + unidade,
            'num_cpfs_per_email_' + str(tempo) + unidade, 'num_cellphones_per_email_' + str(tempo) + unidade,
            'num_ceps_per_email_' + str(tempo) + unidade, 'num_ips_per_email_'+str(tempo)+unidade, 'num_buyernames_per_email_'+str(tempo)+unidade]
    index = dados.ix[:, 'index']
    if unidade == 'day':
        delta = timedelta(days=tempo)
    else:
        delta = timedelta(hours=tempo)
    for i in range(len(dados.index)):
        start = time.time()
        print('bloco email', tempo, unidade, i)
        email = dados['email'][i]
        transaction_date = dados['transaction_date'][i]
        aux = dados[(dados.transaction_date <= transaction_date) & (dados.transaction_date >= (transaction_date - delta))][
            ['card', 'cpf', 'transaction_date', 'cardname', 'email', 'cellphone', 'total_amount', 'cep', 'ip', 'buyer_name']]
        num_trx[i] = len(aux[(aux.email == email)].index)
        amount_trx[i] = aux[(aux.email == email)]['total_amount'].sum()
        card[i] = aux[(aux.email == email)]['card'].nunique()
        cardname[i] = aux[(aux.email == email)]['cardname'].nunique()
        cpf[i] = aux[(aux.email == email)]['cpf'].nunique()
        cellphone[i] = aux[(aux.email == email)]['cellphone'].nunique()
        cep[i] = aux[(aux.email == email)]['cep'].nunique()
        ip[i] = aux[(aux.email == email)]['ip'].nunique()
        buyername[i] = aux[(aux.email == email)]['buyer_name'].nunique()
        end = time.time()
        print(end - start)

    table = zip(index, num_trx,amount_trx, card, cardname, cpf, cellphone, cep, ip, buyername)
    with open('dataset/'+client_name+'features/velocity_variability_email_'+str(tempo)+unidade+'.csv', 'w', newline='') as f:
        writer = csv.writer(f, delimiter = ';')
        writer.writerow(head)
        for row in table:
            writer.writerow(row)

def velocity_variability_cardname(dados, tempo, unidade, client_name):
    num_trx = [0 for i in range(len(dados.index))]
    amount_trx = [0 for i in range(len(dados.index))]
    card = [0 for i in range(len(dados.index))]
    email = [0 for i in range(len(dados.index))]
    cpf = [0 for i in range(len(dados.index))]
    cellphone = [0 for i in range(len(dados.index))]
    cep = [0 for i in range(len(dados.index))]
    ip = [0 for i in range(len(dados.index))]
    buyername = [0 for i in range(len(dados.index))]
    head = ['index', 'num_trx_per_cardname_' + str(tempo) + unidade, 'amount_trx_per_cardname_' + str(tempo) + unidade,
            'num_cards_per_cardname_' + str(tempo) + unidade, 'num_emails_per_cardname_' + str(tempo) + unidade,
            'num_cpfs_per_cardname_' + str(tempo) + unidade, 'num_cellphones_per_cardname_' + str(tempo) + unidade,
            'num_ceps_per_cardname_' + str(tempo) + unidade, 'num_ips_per_cardname_' + str(tempo) + unidade, 'num_buyernames_per_cardname_' + str(tempo) + unidade]
    index = dados.ix[:, 'index']
    if unidade == 'day':
        delta = timedelta(days=tempo)
    else:
        delta = timedelta(hours=tempo)
    for i in range(len(dados.index)):
        start = time.time()
        print('bloco carname', tempo, unidade, i)
        cardname = dados['cardname'][i]
        transaction_date = dados['transaction_date'][i]
        aux = dados[(dados.transaction_date <= transaction_date) & (dados.transaction_date >= (transaction_date - delta))][
            ['card', 'cpf', 'transaction_date', 'cardname', 'email', 'cellphone', 'total_amount', 'cep', 'ip', 'buyer_name']]
        num_trx[i] = len(aux[(aux.cardname == cardname)].index)
        amount_trx[i] = aux[(aux.cardname == cardname)]['total_amount'].sum()
        card[i] = aux[(aux.cardname == cardname)]['card'].nunique()
        email[i] = aux[(aux.cardname == cardname)]['email'].nunique()
        cpf[i] = aux[(aux.cardname == cardname)]['cpf'].nunique()
        cellphone[i] = aux[(aux.cardname == cardname)]['cellphone'].nunique()
        cep[i] = aux[(aux.cardname == cardname)]['cep'].nunique()
        ip[i] = aux[(aux.cardname == cardname)]['ip'].nunique()
        buyername[i] = aux[(aux.cardname == cardname)]['buyer_name'].nunique()
        end = time.time()
        print(end - start)

    table = zip(index, num_trx,amount_trx, card, email, cpf, cellphone, cep, ip, buyername)
    with open('dataset/'+client_name+'features/velocity_variability_cardname_'+str(tempo)+unidade+'.csv', 'w', newline='') as f:
        writer = csv.writer(f, delimiter = ';')
        writer.writerow(head)
        for row in table:
            writer.writerow(row)

def velocity_variability_cellphone(dados, tempo, unidade, client_name):
    num_trx = [0 for i in range(len(dados.index))]
    amount_trx = [0 for i in range(len(dados.index))]
    card = [0 for i in range(len(dados.index))]
    email = [0 for i in range(len(dados.index))]
    cpf = [0 for i in range(len(dados.index))]
    cardname = [0 for i in range(len(dados.index))]
    cep = [0 for i in range(len(dados.index))]
    ip = [0 for i in range(len(dados.index))]
    buyername = [0 for i in range(len(dados.index))]
    head = ['index', 'num_trx_per_cellphone_' + str(tempo) + unidade, 'amount_trx_per_cellphone_' + str(tempo) + unidade,
            'num_cards_per_cellphone_' + str(tempo) + unidade, 'num_emails_per_cellphone_' + str(tempo) + unidade,
            'num_cpfs_per_cellphone_' + str(tempo) + unidade, 'num_cardnames_per_cellphone_' + str(tempo) + unidade,
            'num_ceps_per_cellphone_' + str(tempo) + unidade, 'num_ips_per_cellphone_' + str(tempo) + unidade, 'num_buyernames_per_cellphone_' + str(tempo) + unidade]
    index = dados.ix[:, 'index']
    if unidade == 'day':
        delta = timedelta(days=tempo)
    else:
        delta = timedelta(hours=tempo)
    for i in range(len(dados.index)):
        start = time.time()
        print('bloco cellphone', tempo, unidade, i)
        cellphone = dados['cellphone'][i]
        transaction_date = dados['transaction_date'][i]
        aux = dados[(dados.transaction_date <= transaction_date) & (dados.transaction_date >= (transaction_date - delta))][
            ['card', 'cpf', 'transaction_date', 'cardname', 'email', 'cellphone', 'total_amount', 'cep', 'ip', 'buyer_name']]
        num_trx[i] = len(aux[(aux.cellphone == cellphone)].index)
        amount_trx[i] = aux[(aux.cellphone == cellphone)]['total_amount'].sum()
        card[i] = aux[(aux.cellphone == cellphone)]['card'].nunique()
        email[i] = aux[(aux.cellphone == cellphone)]['email'].nunique()
        cpf[i] = aux[(aux.cellphone == cellphone)]['cpf'].nunique()
        cardname[i] = aux[(aux.cellphone == cellphone)]['cardname'].nunique()
        cep[i] = aux[(aux.cellphone == cellphone)]['cep'].nunique()
        ip[i] = aux[(aux.cellphone == cellphone)]['ip'].nunique()
        buyername[i] = aux[(aux.cellphone == cellphone)]['buyer_name'].nunique()
        end = time.time()
        print(end - start)

    table = zip(index, num_trx,amount_trx, card, email, cpf, cardname, cep, ip, buyername)
    with open('dataset/'+client_name+'features/velocity_variability_cellphone_'+str(tempo)+unidade+'.csv', 'w', newline='') as f:
        writer = csv.writer(f, delimiter = ';')
        writer.writerow(head)
        for row in table:
            writer.writerow(row)

def velocity_variability_cep(dados, tempo, unidade, client_name):
    num_trx = [0 for i in range(len(dados.index))]
    amount_trx = [0 for i in range(len(dados.index))]
    cpf = [0 for i in range(len(dados.index))]
    cardname = [0 for i in range(len(dados.index))]
    email = [0 for i in range(len(dados.index))]
    cellphone = [0 for i in range(len(dados.index))]
    card = [0 for i in range(len(dados.index))]
    ip = [0 for i in range(len(dados.index))]
    buyername = [0 for i in range(len(dados.index))]
    head = ['index', 'num_trx_per_cep_'+str(tempo)+unidade, 'amount_trx_per_cep_'+str(tempo)+unidade,
            'num_cpfs_per_cep_' + str(tempo)+unidade, 'num_cardnames_per_cep_'+str(tempo)+unidade,
            'num_emails_per_cep_' + str(tempo)+unidade, 'num_cellphones_per_cep_'+str(tempo)+unidade,'num_cards_per_cep_'+str(tempo)+unidade,
            'num_ips_per_cep_' + str(tempo) + unidade,'num_buyernames_per_cep_' + str(tempo) + unidade]
    index = dados.ix[:, 'index']
    if unidade == 'day':
        delta = timedelta(days=tempo)
    else:
        delta = timedelta(hours=tempo)
    for i in range(len(dados.index)):
        start = time.time()
        print('bloco cep', tempo, unidade, i)
        cep = dados['cep'][i]
        transaction_date = dados['transaction_date'][i]
        aux = dados[(dados.transaction_date <= transaction_date) & (dados.transaction_date >= (transaction_date - delta))][
                        ['card','cpf','transaction_date','cardname', 'email','cellphone', 'cep', 'total_amount', 'ip', 'buyer_name']]
        num_trx[i] = len(aux[(aux.cep == cep)].index)
        amount_trx[i] = aux[(aux.cep == cep)]['total_amount'].sum()
        cpf[i] = aux[(aux.cep == cep)]['cpf'].nunique()
        cardname[i] = aux[(aux.cep == cep)]['cardname'].nunique()
        email[i] = aux[(aux.cep == cep)]['email'].nunique()
        cellphone[i] = aux[(aux.cep == cep)]['cellphone'].nunique()
        card[i] = aux[(aux.cep == cep)]['card'].nunique()
        ip[i] = aux[(aux.cep == cep)]['ip'].nunique()
        buyername[i] = aux[(aux.cep == cep)]['buyer_name'].nunique()
        end = time.time()
        print(end - start)
    table = zip(index, num_trx,amount_trx, cpf, cardname, email, cellphone, card, ip, buyername)
    with open('dataset/'+client_name+'features/velocity_variability_cep_'+str(tempo)+unidade+'.csv', 'w', newline='') as f:
        writer = csv.writer(f, delimiter=';')
        writer.writerow(head)
        for row in table:
            writer.writerow(row)

def velocity_variability_ip(dados, tempo, unidade, client_name):
    num_trx = [0 for i in range(len(dados.index))]
    amount_trx = [0 for i in range(len(dados.index))]
    cpf = [0 for i in range(len(dados.index))]
    cardname = [0 for i in range(len(dados.index))]
    email = [0 for i in range(len(dados.index))]
    cellphone = [0 for i in range(len(dados.index))]
    card = [0 for i in range(len(dados.index))]
    cep = [0 for i in range(len(dados.index))]
    buyername = [0 for i in range(len(dados.index))]
    head = ['index', 'num_trx_per_ip_'+str(tempo)+unidade, 'amount_trx_per_ip_'+str(tempo)+unidade,
            'num_cpfs_per_ip_' + str(tempo)+unidade, 'num_cardnames_per_ip_'+str(tempo)+unidade,
            'num_emails_per_ip_' + str(tempo)+unidade, 'num_cellphones_per_ip_'+str(tempo)+unidade,'num_cards_per_ip_'+str(tempo)+unidade,
            'num_ceps_per_ip_' + str(tempo) + unidade, 'num_buyernames_per_ip_' + str(tempo) + unidade]
    index = dados.ix[:, 'index']
    if unidade == 'day':
        delta = timedelta(days=tempo)
    else:
        delta = timedelta(hours=tempo)
    for i in range(len(dados.index)):
        start = time.time()
        print('bloco ip', tempo, unidade, i)
        ip = dados['ip'][i]
        transaction_date = dados['transaction_date'][i]
        aux = dados[(dados.transaction_date <= transaction_date) & (dados.transaction_date >= (transaction_date - delta))][
                        ['card','cpf','transaction_date','cardname', 'email','cellphone', 'cep', 'total_amount', 'ip', 'buyer_name']]
        num_trx[i] = len(aux[(aux.ip == ip)].index)
        amount_trx[i] = aux[(aux.ip == ip)]['total_amount'].sum()
        cpf[i] = aux[(aux.ip == ip)]['cpf'].nunique()
        cardname[i] = aux[(aux.ip == ip)]['cardname'].nunique()
        email[i] = aux[(aux.ip == ip)]['email'].nunique()
        cellphone[i] = aux[(aux.ip == ip)]['cellphone'].nunique()
        card[i] = aux[(aux.ip == ip)]['card'].nunique()
        cep[i] = aux[(aux.ip == ip)]['cep'].nunique()
        buyername[i] = aux[(aux.ip == ip)]['buyer_name'].nunique()
        end = time.time()
        print(end - start)
    table = zip(index, num_trx,amount_trx, cpf, cardname, email, cellphone, card, cep, buyername)
    with open('dataset/'+client_name+'features/velocity_variability_ip_'+str(tempo)+unidade+'.csv', 'w', newline='') as f:
        writer = csv.writer(f, delimiter=';')
        writer.writerow(head)
        for row in table:
            writer.writerow(row)

def velocity_variability_session(dados, tempo, unidade, client_name):
    num_trx = [0 for i in range(len(dados.index))]
    amount_trx = [0 for i in range(len(dados.index))]
    cpf = [0 for i in range(len(dados.index))]
    cardname = [0 for i in range(len(dados.index))]
    email = [0 for i in range(len(dados.index))]
    cellphone = [0 for i in range(len(dados.index))]
    card = [0 for i in range(len(dados.index))]
    cep = [0 for i in range(len(dados.index))]
    ip = [0 for i in range(len(dados.index))]
    head = ['index', 'num_trx_per_session_'+str(tempo)+unidade, 'amount_trx_per_session_'+str(tempo)+unidade,
            'num_cpfs_per_session_' + str(tempo)+unidade, 'num_cardnames_per_session_'+str(tempo)+unidade,
            'num_emails_per_session_' + str(tempo)+unidade, 'num_cellphones_per_session_'+str(tempo)+unidade,'num_cards_per_session_'+str(tempo)+unidade,
            'num_ceps_per_session_' + str(tempo) + unidade, 'num_ips_per_session_' + str(tempo) + unidade]
    index = dados.ix[:, 'index']
    if unidade == 'day':
        delta = timedelta(days=tempo)
    else:
        delta = timedelta(hours=tempo)
    for i in range(len(dados.index)):
        start = time.time()
        print('bloco session', tempo, unidade, i)
        session = dados['session'][i]
        transaction_date = dados['transaction_date'][i]
        aux = dados[(dados.transaction_date <= transaction_date) & (dados.transaction_date >= (transaction_date - delta))][
                        ['card','cpf','transaction_date','cardname', 'email','cellphone', 'cep', 'total_amount', 'ip', 'session']]
        num_trx[i] = len(aux[(aux.session == session)].index)
        amount_trx[i] = aux[(aux.session == session)]['total_amount'].sum()
        cpf[i] = aux[(aux.session == session)]['cpf'].nunique()
        cardname[i] = aux[(aux.session == session)]['cardname'].nunique()
        email[i] = aux[(aux.session == session)]['email'].nunique()
        cellphone[i] = aux[(aux.session == session)]['cellphone'].nunique()
        card[i] = aux[(aux.session == session)]['card'].nunique()
        cep[i] = aux[(aux.session == session)]['cep'].nunique()
        ip[i] = aux[(aux.session == session)]['ip'].nunique()
        end = time.time()
        print(end - start)
    table = zip(index, num_trx,amount_trx, cpf, cardname, email, cellphone, card, cep, ip)
    with open('dataset/'+client_name+'features/velocity_variability_ip_'+str(tempo)+unidade+'.csv', 'w', newline='') as f:
        writer = csv.writer(f, delimiter=';')
        writer.writerow(head)
        for row in table:
            writer.writerow(row)

def signals_historico(dados, variavel, client_name):
    cbk = [0 for i in range(len(dados.index))]
    days_since_first_transaction = [0 for i in range(len(dados.index))]
    head = ['index', 'num_cbk_per_'+variavel, 'days_since_first_trasaction_'+variavel]
    index = dados.ix[:, 'index']
    if variavel == 'email':
        for i in range(len(dados.index)):
            start = time.time()
            print('bloco historico', variavel, i)
            email = dados['email'][i]
            transaction_date = dados['transaction_date'][i]
            cbk[i] = len(dados[dados.cbk & (dados.incoming_date < transaction_date) & (
            email == dados.email)].index)
            days_since_first_transaction[i] = (transaction_date - dados[(email == dados.email)]['transaction_date'].min()).days
            end = time.time()
            print(end - start)
    elif variavel == 'card':
        for i in range(len(dados.index)):
            start = time.time()
            print('bloco historico', variavel, i)
            card = dados['card'][i]
            transaction_date = dados['transaction_date'][i]
            cbk[i] = len(dados[dados.cbk & (dados.incoming_date < transaction_date) & (
            card == dados.card)].index)
            days_since_first_transaction[i] = (
            transaction_date - dados[(card == dados.card)]['transaction_date'].min()).days
            end = time.time()
            print(end - start)
    elif variavel == 'cpf':
        for i in range(len(dados.index)):
            start = time.time()
            print('bloco historico', variavel, i)
            cpf = dados['cpf'][i]
            transaction_date = dados['transaction_date'][i]
            cbk[i] = len(dados[dados.cbk & (dados.incoming_date < transaction_date) & (
            cpf == dados.cpf)].index)
            days_since_first_transaction[i] = (
            transaction_date - dados[(cpf == dados.cpf)]['transaction_date'].min()).days
            end = time.time()
            print(end - start)
    elif variavel == 'cardname':
        for i in range(len(dados.index)):
            start = time.time()
            print('bloco historico', variavel, i)
            cardname = dados['cardname'][i]
            transaction_date = dados['transaction_date'][i]
            cbk[i] = len(dados[dados.cbk & (dados.incoming_date < transaction_date) & (
            cardname == dados.cardname)].index)
            days_since_first_transaction[i] = (
                transaction_date - dados[(cardname == dados.cardname)]['transaction_date'].min()).days
            end = time.time()
            print(end - start)
    elif variavel == 'cellphone':
        for i in range(len(dados.index)):
            start = time.time()
            print('bloco historico', variavel, i)
            cellphone = dados['cellphone'][i]
            transaction_date = dados['transaction_date'][i]
            cbk[i] = len(dados[dados.cbk & (dados.incoming_date < transaction_date) & (
            cellphone == dados.cellphone)].index)
            days_since_first_transaction[i] = (
            transaction_date - dados[(cellphone == dados.cellphone)]['transaction_date'].min()).days
            end = time.time()
            print(end - start)
    elif variavel == 'cep':
        for i in range(len(dados.index)):
            start = time.time()
            print('bloco historico', variavel, i)
            cep = dados['cep'][i]
            transaction_date = dados['transaction_date'][i]
            cbk[i] = len(dados[dados.cbk & (dados.incoming_date < transaction_date) & (cep == dados.cep)].index)
            days_since_first_transaction[i] = (
            transaction_date - dados[(cep == dados.cep)]['transaction_date'].min()).days
            end = time.time()
            print(end - start)
    elif variavel == 'bin_cpf':
        dados['bin'] = dados.card.str[:6]
        dados['bin_cpf'] = dados.bin + dados.cpf
        for i in range(len(dados.index)):
            start = time.time()
            print('bloco historico', variavel, i)
            bin_cpf = dados['bin_cpf'][i]
            transaction_date = dados['transaction_date'][i]
            cbk[i] = len(dados[dados.cbk & (dados.incoming_date < transaction_date) & (
            bin_cpf == dados.bin_cpf)].index)
            days_since_first_transaction[i] = (
            transaction_date - dados[(bin_cpf == dados.bin_cpf)]['transaction_date'].min()).days
            end = time.time()
            print(end - start)
    elif variavel == 'card_cpf':
        dados['card_cpf'] = dados.card + dados.cpf
        for i in range(len(dados.index)):
            start = time.time()
            print('bloco historico', variavel, i)
            card_cpf = dados['card_cpf'][i]
            transaction_date = dados['transaction_date'][i]
            cbk[i] = len(dados[dados.cbk & (dados.incoming_date < transaction_date) & (
                card_cpf == dados.card_cpf)].index)
            days_since_first_transaction[i] = (
                transaction_date - dados[(card_cpf == dados.card_cpf)]['transaction_date'].min()).days
            end = time.time()
            print(end - start)
    else:
        print('Variável inválida')

    table = zip(index, cbk, days_since_first_transaction)
    with open('dataset/'+client_name+'features/signals_historico_'+variavel+'.csv', 'w', newline='') as f:
        writer = csv.writer(f, delimiter = ';')
        writer.writerow(head)
        for row in table:
            writer.writerow(row)
