import csv
import json
from flatten_json import flatten
from elasticsearch import Elasticsearch, helpers
import time
from datetime import datetime
import pandas as pd
from pandas.io.json import json_normalize


# def risco_bairro(dados):
host = '54.167.197.209'
es = Elasticsearch([{'host': host, 'port': 9200}], http_auth="elastic:xupaclearsale")
# for i in range(len(dados.index)):
# neighborhood = dados['neighborhood'][i]
neighborhood = 'Caetano'
try:
    neighborhood = neighborhood.lower()
    empty = False
except AttributeError:
    empty = True

cpf_list = ['06378849900', '83884297872', '00609477803', '56885892726', '08100639728', '06378849900', '83884297872', '00609477803', '56885892726', '08100639728']
cbk = list()

start = time.time()
for cpf in cpf_list:
    query = {
        "query": {
            "bool": {
                "must": [
                    {"match": {
                        "customer.documents.number.keyword": str(cpf)
                    }},
                    {"match": {
                        "payment.status.keyword": "chargeback"
                    }}
                ]
            }
        },
        "size": 0,
        "aggs": {
            "total_amount": {
                "sum": {
                    "field": "payment.total_amount"
                }
            }
        }
    }

    cbk.append(es.search(index='order_v1', body=query))
end = time.time()
print(cbk)
print(end-start)

