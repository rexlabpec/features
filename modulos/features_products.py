import numpy as np
import pandas as pd
from datetime import timedelta
import csv
import time


bebidas = ['cerveja', 'vodka', 'jurupinga', 'catuaba', 'whisk', 'chopp', 'vinho', 'smirnoff', 'skol', 'itaipava', 'absolut', 'long neck', 'black label']

def retirar_acentos(text):
    text = text.replace('á', 'a')
    text = text.replace('é', 'e')
    text = text.replace('í', 'i')
    text = text.replace('ó', 'o')
    text = text.replace('ú', 'u')

    text = text.replace('à', 'a')
    text = text.replace('è', 'e')
    text = text.replace('ì', 'i')
    text = text.replace('ò', 'o')
    text = text.replace('ù', 'u')

    text = text.replace('ã', 'a')
    text = text.replace('ẽ', 'e')
    text = text.replace('ĩ', 'i')
    text = text.replace('õ', 'o')
    text = text.replace('ũ', 'u')

    text = text.replace('â', 'a')
    text = text.replace('ê', 'e')
    text = text.replace('î', 'i')
    text = text.replace('ô', 'o')
    text = text.replace('û', 'u')

    text = text.replace('ä', 'a')
    text = text.replace('ë', 'e')
    text = text.replace('ï', 'i')
    text = text.replace('ö', 'o')
    text = text.replace('ü', 'u')

    text = text.replace('ç', 'c')
    text = text.replace('ñ', 'n')

    text = text.replace('Á', 'A')
    text = text.replace('É', 'E')
    text = text.replace('Í', 'I')
    text = text.replace('Ó', 'O')
    text = text.replace('Ú', 'U')

    text = text.replace('À', 'A')
    text = text.replace('È', 'E')
    text = text.replace('Ì', 'I')
    text = text.replace('Ò', 'O')
    text = text.replace('Ù', 'U')

    text = text.replace('Ã', 'A')
    text = text.replace('Ẽ', 'E')
    text = text.replace('Ĩ', 'I')
    text = text.replace('Õ', 'O')
    text = text.replace('Ũ', 'U')

    text = text.replace('Â', 'A')
    text = text.replace('Ê', 'E')
    text = text.replace('Î', 'I')
    text = text.replace('Ô', 'O')
    text = text.replace('Û', 'U')

    text = text.replace('Ä', 'A')
    text = text.replace('Ë', 'E')
    text = text.replace('Ï', 'I')
    text = text.replace('Ö', 'O')
    text = text.replace('Ü', 'U')

    text = text.replace('Ç', 'C')
    text = text.replace('Ñ', 'N')

    return text


def features_carrinho(dados, client_name):
    alcool = [0 for i in range(len(dados.index))]
    so_alcool = [0 for i in range(len(dados.index))]
    quantidade_alcool_percent = [0 for i in range(len(dados.index))]
    valor_alcool_percent = [0 for i in range(len(dados.index))]
    quantidade_itens = [0 for i in range(len(dados.index))]
    max_unit_cost = [0 for i in range(len(dados.index))]
    head = ['index', 'alcohol', 'only_alcohol', 'quantity_alcohol', 'amount_alcohol', 'total_quantity', 'max_unit_cost']
    index = dados.ix[:, 'index']
    for i in range(len(dados.index)):
        products = dados['products'][i]
        quantidade = 0
        valor = 0
        quantidade_alcool = 0
        valor_alcool = 0
        max_unit_cost = 0
        for product in products:
            item = retirar_acentos(product.get('name')).lower()
            quantidade += product.get('quantity')
            valor += (product.get('unit_cost')*product.get('quantity'))
            for bebida in bebidas:
                if bebida in item:
                    quantidade_alcool += product.get('quantity')
                    valor_alcool += (product.get('unit_cost')*product.get('quantity'))
            if product.get('unit_cost') >= max_unit_cost:
                max_unit_cost[i] = product.get('unit_cost')
        if quantidade_alcool > 0:
            alcool[i] = True
        else:
            alcool[i] = False
        if quantidade_alcool == quantidade:
            so_alcool[i] = True
        else:
            so_alcool[i] = False
        quantidade_alcool_percent[i] = quantidade_alcool/quantidade
        valor_alcool_percent[i] = valor_alcool/valor
        quantidade_itens[i] = quantidade
    table = zip(index, alcool, so_alcool, quantidade_alcool_percent, valor_alcool_percent, quantidade_itens, max_unit_cost)
    with open(client_name+'/features/features_carrinho.csv', 'w', newline='') as f:
        writer = csv.writer(f, delimiter=';')
        writer.writerow(head)
        for row in table:
            writer.writerow(row)

