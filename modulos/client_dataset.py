import pandas as pd
import psycopg2


def client_dataset(client_name, vertical, user, password):
    endpoint = 'rexlab-scooby-replica-a.cujsiitttsed.us-east-1.rds.amazonaws.com'
    db = 'ScoobyDB'

    conn = psycopg2.connect(host=endpoint, database=db, user=user, password=password)
    with open ("../query/"+vertical+".sql","r") as myfile:
        query = " ".join(line.strip() for line in myfile if not line.startswith("--"))
    myfile.close()
    query = query + " AND "+ '"Client".name LIKE ' +"'%" + client_name + "%' ORDER BY " + '"Order".order_date DESC'
    print("Query carregada")
    dados = pd.read_sql_query(query, con=conn)
    print("Arrumando dados")
    dados['cbk'] = False
    dados.loc[dados.Issuer_Decision == 'Chargeback','cbk'] = True
    dados['incoming_date'] = pd.to_datetime(dados['updated_date'])
    dados['incoming_date'] = dados['incoming_date'].fillna(dados['transaction_date'].min())
    dados['cellphone'] = dados['ddi'].astype(str)+dados['ddd'].astype(str)+dados['cell_number'].astype(str)
    dados['card'] = dados['bin'].astype(str)+dados['last_4'].astype(str)+dados['exp_date'].astype(str)
    print(dados)
    dados.to_csv('dataset/'+client_name+'/dados.csv', sep=';', encoding='utf-8', index=False)
    dados = dados.drop(['ddi', 'ddd', 'cell_number', 'bin', 'last_4', 'exp_date', 'Order_ID', 'Client_Name', 'reasons',
                        'order_key', 'seller_name', 'seller_id', 'platform', 'updated_date'], axis=1)
    return dados

a = client_dataset('Pedidos', 'products', 'pchen', 'vA6KZiRPNCRVbpQP')