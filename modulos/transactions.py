
def retirar_acentos(text):
    text = text.replace('á', 'a')
    text = text.replace('é', 'e')
    text = text.replace('í', 'i')
    text = text.replace('ó', 'o')
    text = text.replace('ú', 'u')

    text = text.replace('à', 'a')
    text = text.replace('è', 'e')
    text = text.replace('ì', 'i')
    text = text.replace('ò', 'o')
    text = text.replace('ù', 'u')

    text = text.replace('ã', 'a')
    text = text.replace('ẽ', 'e')
    text = text.replace('ĩ', 'i')
    text = text.replace('õ', 'o')
    text = text.replace('ũ', 'u')

    text = text.replace('â', 'a')
    text = text.replace('ê', 'e')
    text = text.replace('î', 'i')
    text = text.replace('ô', 'o')
    text = text.replace('û', 'u')

    text = text.replace('ä', 'a')
    text = text.replace('ë', 'e')
    text = text.replace('ï', 'i')
    text = text.replace('ö', 'o')
    text = text.replace('ü', 'u')

    text = text.replace('ç', 'c')
    text = text.replace('ñ', 'n')

    text = text.replace('Á', 'A')
    text = text.replace('É', 'E')
    text = text.replace('Í', 'I')
    text = text.replace('Ó', 'O')
    text = text.replace('Ú', 'U')

    text = text.replace('À', 'A')
    text = text.replace('È', 'E')
    text = text.replace('Ì', 'I')
    text = text.replace('Ò', 'O')
    text = text.replace('Ù', 'U')

    text = text.replace('Ã', 'A')
    text = text.replace('Ẽ', 'E')
    text = text.replace('Ĩ', 'I')
    text = text.replace('Õ', 'O')
    text = text.replace('Ũ', 'U')

    text = text.replace('Â', 'A')
    text = text.replace('Ê', 'E')
    text = text.replace('Î', 'I')
    text = text.replace('Ô', 'O')
    text = text.replace('Û', 'U')

    text = text.replace('Ä', 'A')
    text = text.replace('Ë', 'E')
    text = text.replace('Ï', 'I')
    text = text.replace('Ö', 'O')
    text = text.replace('Ü', 'U')

    text = text.replace('Ç', 'C')
    text = text.replace('Ñ', 'N')

    return text


def retirar_aspas(text):
    if text:
        text = text.replace('"', '')
        return text
    return None

class ChargebackGerencial:
    def __init__(self, data):
        self.trx_id = data[0]
        self.city = data[1].lower() if data[1] else None
        self.state = data[2].lower() if data[2] else None
        self.street = data[3].lower() if data[3] else None
        self.street_number = data[4].lower() if data[4] else None
        self.neighborhood = data[5].lower() if data[5] else None
        self.zip_code = retirar_aspas(data[6]) if data[6] else None
        self.document_type = data[7].lower() if data[7] else 'cpf'
        self.document_number = data[8]
        self.email = data[9].lower() if data[9] else None
        self.customer_name = retirar_acentos(data[10].lower())
        self.phone_ddi = data[11] if data[11] else 'missing'
        self.phone_ddd = data[12] if data[12] else 'missing'
        self.phone_number = data[13] if data[13] else 'missing'
        self.ip = data[14]
        self.card_name = data[15].lower()
        self.bin = data[16]
        self.last_4 = data[17]
        self.expiration_date = data[18]
        self.incoming_date = data[19] if data[19] else '2017-07-08T20:44:18.000Z'

    def get_fullnumber(self):
        if self.phone_number == 'missing':
            return 'missing'
        return str(self.phone_ddi) + str(self.phone_ddd) + str(self.phone_number)

    def get_cardhash_complete(self):
        return str(self.bin) + str(self.last_4) + str(self.expiration_date)

    def get_cardhash_bin_last4(self):
        return str(self.bin) + '****' + str(self.last_4)


# Any csv file
class TransactionCsv:
    def __init__(self, data): # data is your csv file and data[i] represent the column i
        self.client = data[0]
        self.trx_id = data[1]
        self.status = data[2].lower()
        self.created_at = data[3]
        self.customer_name = retirar_acentos(data[4].lower())
        self.gender = data[5]
        self.document_number = str(data[6])
        self.cellphone = data[7]
        self.telephone = data[8]
        self.email = data[9].lower()
        self.ip = data[10]
        self.shipping_address = data[11]
        self.shopping_cart = data[12]
        self.amount = float(data[13])/100
        self.installments = data[14]
        self.card_id = data[15]
        self.card_name = data[16].lower()
        self.retry = data[17]
        self.zip_code = data[18]

        self.document_type = 'cpf' if len(self.document_number) == 11 else 'cnpj'

#Gerencial
class TransactionGerencial:
    def __init__(self, data):
        self.trx_id = data[0]
        self.client = data[1]
        self.created_at = data[3]
        self.amount = data[4]
        self.issuer_decision = data[6]
        self.updated_at = data[7]
        self.seller = data[12]
        self.city = data[13].lower() if data[13] else None
        self.state = data[14].lower() if data[14] else None
        self.street = data[15].lower() if data[15] else None
        self.street_number = data[16]
        self.neighborhood = data[17].lower() if data[16] else None
        self.zip_code = retirar_aspas(data[18])
        self.document_type = data[19].lower() if data[13] else 'cpf'
        self.document_number = str(data[20]).rjust(11, '0')
        self.email = data[21].lower() if data[21] else None
        self.customer_name = data[22]
        self.phone_ddi = retirar_aspas(data[23])
        self.phone_ddd = retirar_aspas(data[24])
        self.phone_number = retirar_aspas(data[25])
        self.ip = data[26]
        self.platform = data[27].lower() if data[27] else None
        self.session_id = data[28]
        self.card_name = data[29].lower() if data[13] else None
        self.bin = data[30]
        self.last_4 = data[31]
        self.expiration_date = data[32]
        self.products = data[33] if data[33] else None
        self.af_decision_id = data[34]

        if data[6].lower() == 'chargeback':
            self.chargeback = 1
            self.status = 'Captured'
        elif data[6].lower() == 'cancelled':
            self.chargeback = 0
            self.status = 'Refunded'
        elif data[6].lower() == 'not authorized':
            self.chargeback = 0
            if data[34] == 8:
                self.status = 'Refused'
            else:
                self.status = 'NotAuthorized'
        else:
            self.chargeback = 0
            self.status = 'Captured'

    def get_fullnumber(self):
        if self.phone_number and self.phone_ddd and self.phone_ddi:
            return str(self.phone_ddi) + str(self.phone_ddd) + str(self.phone_number)
        return None

    def get_cardhash(self):
        return str(self.bin)+str(self.last_4)+str(self.expiration_date)

#Banco PagarMe
class TransactionPagarme:
    def __init__(self, data):
        self.trx_id             = data[0]
        self.antifraud_score    = data[1]
        self.antifraud_provider = data[2]
        self.status             = data[3]
        self.status_reason      = data[4]
        self.created_at         = data[5]
        self.updated_at         = data[6]
        self.company_id         = data[7]  if data[7]  else 'missing'
        self.amount             = data[8]
        self.card_holder_name   = data[9]
        self.card_id            = data[10] if data[10] else 'missing'
        self.name               = data[11]
        self.email              = data[12] if data[12] else 'missing'
        self.born_at            = data[13]
        self.document_type      = data[14] if data[14] else 'missing'
        self.document_number    = data[15] if data[15] else 'missing'
        self.phone_ddi          = data[16]
        self.phone_ddd          = data[17]
        self.phone_number       = data[18]
        self.street             = data[19]
        self.street_number      = data[20]
        self.neighborhood       = data[21]
        self.city               = data[22]
        self.state              = data[23]
        self.zipcode            = data[24]
        self.country            = data[25]
        self.card_country       = data[26]
        self.card_fist_digits   = data[27]
        self.card_last_four     = data[28]
        self.card_exp_date      = data[29]
        self.cb_accrual_date    = data[30]
        self.cb_created_at      = data[31]
        self.cb_reason_code     = data[32]
        self.cb_status          = data[33]


    def get_fullnumber(self):
        return self.phone_ddi + self.phone_ddd + self.phone_number
