import calculate_features

def main_csv():
    calculate_features.process_data_csv('dataset/dados')
    finish = input('Finish')

def main_gerencial():
    client_name = 'Pedidos'
    vertical = 'products'
    calculate_features.connect()
    calculate_features.fetch_data(client_name, vertical, None, None)
    calculate_features.process_data_gerencial('dataset/'+client_name)
    finish = input('Finish')
    calculate_features.disconnect()

print('Initialize global variables')
calculate_features.history = None
calculate_features.chargeback = None
calculate_features.features = None
calculate_features.bureau = None
main_gerencial()
