import numpy as np
import pandas as pd
from datetime import timedelta
import csv
import time


def cpf_is_valid(dados):
    # coluna cpf deve ser string e conter apenas números
    cpf_is_valid = [0 for i in range(len(dados.index))]
    head = ['index', 'cpf_is_valid']
    index = dados.ix[:, 'index']
    for i in range(len(dados.index)):
        cpf = dados['cpf'][i]
        if (not cpf) or (len(cpf) != 11 and len(cpf) != 14):
            cpf_is_valid[i] = False
        elif len(cpf) == 11:
            inteiros = list(map(int, cpf))
            novo = inteiros[:9]
            while len(novo) < 11:
                r = sum([(len(novo) + 1 - i) * v for i, v in enumerate(novo)]) % 11
                if r > 1:
                    f = 11 - r
                else:
                    f = 0
                novo.append(f)
            if novo == inteiros:
                cpf_is_valid[i] = True
            else:
                cpf_is_valid[i] = False
        elif len(cpf) == 14:
            inteiros = list(map(int, cpf))
            novo = inteiros[:12]
            prod = [5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2]
            while len(novo) < 14:
                r = sum([x * y for (x, y) in zip(novo, prod)]) % 11
                if r > 1:
                    f = 11 - r
                else:
                    f = 0
                novo.append(f)
                prod.insert(0, 6)
            if novo == inteiros:
                cpf_is_valid[i] = True
            else:
                cpf_is_valid[i] = False
        else:
            cpf_is_valid[i] = False

    table = zip(index, cpf_is_valid)
    with open('features/is_valid_cpf.csv', 'w', newline='') as f:
        writer = csv.writer(f, delimiter=';')
        writer.writerow(head)
        for row in table:
            writer.writerow(row)

def comparar_origem_cpf(dados):
    origens_cpf = ['rs', ['df', 'go', 'ms', 'to'], ['pa', 'am', 'ac', 'ap', 'ro', 'rr'], ['ce', 'ma', 'pi'], ['pe', 'rn', 'pb', 'al'], ['ba', 'se'], 'mg', ['rj', 'es'], 'sp', ['pr', 'sc']]
    ddd_estado = {
        "99": "MA",
        "98": "MA",
        "97": "AM",
        "96": "AP",
        "95": "RR",
        "94": "PA",
        "93": "PA",
        "92": "AM",
        "91": "PA",
        "88": "CE",
        "87": "PE",
        "86": "PI",
        "85": "CE",
        "83": "PB",
        "82": "AL",
        "81": "PE",
        "79": "SE",
        "77": "BA",
        "75": "BA",
        "74": "BA",
        "73": "BA",
        "72": "BA",
        "71": "BA",
        "69": "RO",
        "68": "AC",
        "67": "MS",
        "66": "MT",
        "65": "MT",
        "64": "GO",
        "63": "TO",
        "62": "GO",
        "61": "DF",
        "55": "RS",
        "54": "RS",
        "53": "RS",
        "51": "RS",
        "49": "SC",
        "48": "SC",
        "47": "SC",
        "46": "PR",
        "45": "PR",
        "44": "PR",
        "43": "PR",
        "42": "PR",
        "41": "PR",
        "38": "MG",
        "37": "MG",
        "35": "MG",
        "34": "MG",
        "33": "MG",
        "32": "MG",
        "31": "MG",
        "28": "ES",
        "27": "ES",
        "24": "RJ",
        "22": "RJ",
        "21": "RJ",
        "19": "SP",
        "18": "SP",
        "17": "SP",
        "16": "SP",
        "15": "SP",
        "14": "SP",
        "13": "SP",
        "12": "SP",
        "11": "SP"
    }
    shipping_state = [0 for i in range(len(dados.index))]
    same_ddd_cpf_state = [0 for i in range(len(dados.index))]
    head = ['index', 'same_shipping_cpf_state', 'same_ddd_cpf_state']
    index = dados.ix[:, 'index']
    for i in range(len(dados.index)):
        cpf = dados['cpf'][i]
        cpf = cpf.rjust(11, '0')
        ddd = dados['cellphone'][i][2:4]
        shipping_state[i] = dados['state'][i].lower() in origens_cpf[int(cpf[8])]
        same_ddd_cpf_state[i] = dados['state'][i].lower() == ddd_estado[ddd].lower()
    table = zip(index, shipping_state, same_ddd_cpf_state)
    with open('features/comparacao_origem_cpf.csv', 'w', newline='') as f:
        writer = csv.writer(f, delimiter=';')
        writer.writerow(head)
        for row in table:
            writer.writerow(row)
