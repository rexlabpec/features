import numpy as np
import pandas as pd
from datetime import timedelta
import csv
import time
import re

def retirar_acentos(text):
    text = text.replace('á', 'a')
    text = text.replace('é', 'e')
    text = text.replace('í', 'i')
    text = text.replace('ó', 'o')
    text = text.replace('ú', 'u')

    text = text.replace('à', 'a')
    text = text.replace('è', 'e')
    text = text.replace('ì', 'i')
    text = text.replace('ò', 'o')
    text = text.replace('ù', 'u')

    text = text.replace('ã', 'a')
    text = text.replace('ẽ', 'e')
    text = text.replace('ĩ', 'i')
    text = text.replace('õ', 'o')
    text = text.replace('ũ', 'u')

    text = text.replace('â', 'a')
    text = text.replace('ê', 'e')
    text = text.replace('î', 'i')
    text = text.replace('ô', 'o')
    text = text.replace('û', 'u')

    text = text.replace('ä', 'a')
    text = text.replace('ë', 'e')
    text = text.replace('ï', 'i')
    text = text.replace('ö', 'o')
    text = text.replace('ü', 'u')

    text = text.replace('ç', 'c')
    text = text.replace('ñ', 'n')

    text = text.replace('Á', 'A')
    text = text.replace('É', 'E')
    text = text.replace('Í', 'I')
    text = text.replace('Ó', 'O')
    text = text.replace('Ú', 'U')

    text = text.replace('À', 'A')
    text = text.replace('È', 'E')
    text = text.replace('Ì', 'I')
    text = text.replace('Ò', 'O')
    text = text.replace('Ù', 'U')

    text = text.replace('Ã', 'A')
    text = text.replace('Ẽ', 'E')
    text = text.replace('Ĩ', 'I')
    text = text.replace('Õ', 'O')
    text = text.replace('Ũ', 'U')

    text = text.replace('Â', 'A')
    text = text.replace('Ê', 'E')
    text = text.replace('Î', 'I')
    text = text.replace('Ô', 'O')
    text = text.replace('Û', 'U')

    text = text.replace('Ä', 'A')
    text = text.replace('Ë', 'E')
    text = text.replace('Ï', 'I')
    text = text.replace('Ö', 'O')
    text = text.replace('Ü', 'U')

    text = text.replace('Ç', 'C')
    text = text.replace('Ñ', 'N')

    return text

def porcentagem_tipos_caracteres(dados, variavel, client_name):
    vowel = [0 for i in range(len(dados.index))]
    consonant = [0 for i in range(len(dados.index))]
    number = [0 for i in range(len(dados.index))]
    special = [0 for i in range(len(dados.index))]
    unique = [0 for i in range(len(dados.index))]
    head = ['index', 'vowel_percent_'+variavel, 'consonant_percent_'+variavel, 'numeric_percent_'+variavel, 'special_percent_'+variavel, 'unique_characters_'+variavel]
    index = dados.ix[:, 'index']
    vowels = 'aeiou'
    consonants = 'bcdfghjklmnpqrstvwxyz'
    numbers = '0123456789'
    if  variavel != 'email' and variavel != 'buyer_name' and variavel != 'cardname':
        print('Variavel invalida')
        return False
    for i in range(len(dados.index)):
        print('porcentagem email -',i)
        if variavel == 'email':
            string = dados['email'][i]
            try:
                string = string.rsplit('@')[0].lower()
            except AttributeError:
                string = ' '
        elif variavel == 'buyer_name':
            string = dados['buyer_name'][i]
            string = retirar_acentos(string)
        elif variavel == 'cardname':
            string = dados['cardname'][i]
            string = retirar_acentos(string)
        else:
            string = ''
        vowel[i] = sum(string.count(i) for i in vowels)/len(string)
        consonant[i] = sum(string.count(i) for i in consonants)/len(string)
        number[i] = sum(string.count(i) for i in numbers)/len(string)
        special[i] = 1 - vowel[i] - consonant[i] - number[i]
        unique[i] = len(list(set(string)))
    table = zip(index, vowel, consonant, number, special, unique)
    with open('dataset/'+client_name+'features/porcentagem_de_caracteres_'+variavel+'.csv', 'w', newline='') as f:
        writer = csv.writer(f, delimiter=';')
        writer.writerow(head)
        for row in table:
            writer.writerow(row)

def match_names(dados, client_name):
    first_names = [0 for i in range(len(dados.index))]
    last_names = [0 for i in range(len(dados.index))]
    first_buyer_name = [0 for i in range(len(dados.index))]
    last_buyer_name = [0 for i in range(len(dados.index))]
    first_card_name = [0 for i in range(len(dados.index))]
    last_card_name = [0 for i in range(len(dados.index))]
    head = ['index', 'first_name_match_in_cardname', 'last_name_match_in_cardname', 'first_buyer_name_in_email',
            'last_buyer_name_in_email', 'first_card_name_in_email', 'last_card_name_in_email']
    index = dados.ix[:, 'index']
    for i in range(len(dados.index)):
        buyer = retirar_acentos(dados['buyer_name'][i])
        cardname = retirar_acentos(dados['cardname'][i])
        email = dados['email'][i]
        try:
            email = email.lower()
        except AttributeError:
            email = ''
        buyer = buyer.lower().rsplit(' ')
        cardname =  cardname.lower().rsplit(' ')
        print("match name -", i)
        # match buyer and cardname
        if re.search(buyer[0], dados['cardname'][i].lower()):
            first_names[i] = True
        else:
            first_names[i] = False
        if re.search(buyer[len(buyer)-1], dados['cardname'][i].lower()):
            last_names[i] = True
        else:
            last_names[i] = False

        # buyer name in email
        if re.search(buyer[0], email):
            first_buyer_name[i] = True
        else:
            first_buyer_name[i] = False
        if re.search(buyer[len(buyer)-1], email):
            last_buyer_name[i] = True
        else:
            last_buyer_name[i] = False

        #carname in email
        if re.search(cardname[0], email):
            first_card_name[i] = True
        else:
            first_card_name[i] = False
        if re.search(cardname[len(cardname)-1], email):
            last_card_name[i] = True
        else:
            last_card_name[i] = False

    table = zip(index, first_names, last_names, first_buyer_name, last_buyer_name, first_card_name, last_card_name)
    with open('dataset/'+client_name+'features/match_names.csv', 'w', newline='') as f:
        writer = csv.writer(f, delimiter=';')
        writer.writerow(head)
        for row in table:
            writer.writerow(row)